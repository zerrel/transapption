package com.infosupport.transapption.mapper;

import com.infosupport.transapption.model.dto.BankAccountDto;
import com.infosupport.transapption.model.entity.BankAccount;
import com.infosupport.transapption.model.entity.User;
import com.infosupport.transapption.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.Silent.class)
public class BankAccountMapperUnitTest {

    private BankAccount bankAccount;
    private BankAccountDto bankAccountDto;
    private List<BankAccount> bankAccounts;
    private List<BankAccountDto> bankAccountDtos;

    @Mock
    private UserService userService;

    @InjectMocks
    private BankAccountMapper bankAccountMapper = Mappers.getMapper(BankAccountMapper.class);

    @Before
    public void init() {
        final User accountHolder = new User(1L, "Username123", "Password123!", "ROLE_USER");

        bankAccount = new BankAccount(accountHolder, 100L, 0L, "BankAccount description");
        bankAccountDto = new BankAccountDto(1L, 100L, 0L, "description");

        bankAccounts = new ArrayList<>();
        bankAccountDtos = new ArrayList<>();

        bankAccounts.add(bankAccount);
        bankAccountDtos.add(bankAccountDto);

        Mockito.when(userService.getUserById(any(Long.class)))
                .thenReturn(accountHolder);
    }

    /*
        Test whether the right attributes are converted
        from Dto to Entity
     */
    @Test
    public void bankAccountDtoToEntityHappyPath() {
        BankAccount target = bankAccountMapper.toEntity(bankAccountDto);
        // assert that BankAccount has been initialized
        assertNotNull(target);
        // attributes that should be included
        assertEquals(bankAccountDto.getDescription(), target.getDescription());
        assertEquals(bankAccountDto.getAccountHolderId(), target.getAccountHolder().getUserId());
        // attributes that should be excluded
        assertEquals(bankAccountDto.getBankAccountId(), target.getBankAccountId());
        assertEquals(bankAccountDto.getMinimumBalance(), target.getMinimumBalance());
        assertEquals(bankAccountDto.getBalance(), target.getBalance());
    }

    /*
        Test whether the right attributes are converted
        from Entity to Dto
     */
    @Test
    public void bankAccountEntityToDtoHappyPath() {
        BankAccountDto target = bankAccountMapper.toDto(bankAccount);
        // assert that BankAccountDto has been initialized
        assertNotNull(target);
        // attributes that should be included
        assertEquals(bankAccount.getBankAccountId(), target.getBankAccountId());
        assertEquals(bankAccount.getAccountHolder().getUserId(), target.getAccountHolderId());
        assertEquals(bankAccount.getDescription(), target.getDescription());
        // attributes that should be excluded
        assertNull(target.getBalance());
        assertNull(target.getMinimumBalance());
    }

    /*
        Test whether the right attributes are converted
        from Entity to private Dto
     */
    @Test
    public void bankAccountEntityToPrivateDtoHappyPath() {
        BankAccountDto target = bankAccountMapper.toPrivateDto(bankAccount);
        // assert that BankAccountDto has been initialized;
        assertNotNull(target);
        // attributes that should be included
        assertEquals(bankAccount.getBankAccountId(), target.getBankAccountId());
        assertEquals(bankAccount.getAccountHolder().getUserId(), target.getAccountHolderId());
        assertEquals(bankAccount.getBalance(), target.getBalance());
        assertEquals(bankAccount.getMinimumBalance(), target.getMinimumBalance());
        assertEquals(bankAccount.getDescription(), target.getDescription());
    }

    /*
        Test whether the account holder id is null
        when passing no account holder
     */
    @Test
    public void bankAccountEntityToDtoWithoutAccountHolder() {
        // remove account holder
        bankAccount.setAccountHolder(null);
        // run the method to be tested
        BankAccountDto target = bankAccountMapper.toDto(bankAccount);
        // attributes that should be included
        assertEquals(bankAccount.getBankAccountId(), target.getBankAccountId());
        assertEquals(bankAccount.getDescription(), target.getDescription());
        // attributes that should be excluded
        assertNull(target.getAccountHolderId());
        assertNull(target.getBalance());
        assertNull(target.getMinimumBalance());
    }

    /*
       Test whether the account holder id is null
       when passing no account holder id
    */
    @Test
    public void bankAccountEntityToDtoWithoutAccountHolderId() {
        // remove account holder
        bankAccount.getAccountHolder().setUserId(null);
        // run the method to be tested
        BankAccountDto target = bankAccountMapper.toDto(bankAccount);
        // attributes that should be included
        assertEquals(bankAccount.getBankAccountId(), target.getBankAccountId());
        assertEquals(bankAccount.getDescription(), target.getDescription());
        // attributes that should be excluded
        assertNull(target.getAccountHolderId());
        assertNull(target.getBalance());
        assertNull(target.getMinimumBalance());
    }

    /*
       Test whether the account holder id is null
       when passing no account holder id
    */
    @Test
    public void bankAccountEntityToPrivateDtoWithoutAccountHolderId() {
        // remove account holder
        bankAccount.getAccountHolder().setUserId(null);
        // run the method to be tested
        BankAccountDto target = bankAccountMapper.toPrivateDto(bankAccount);
        // attributes that should be included
        assertEquals(bankAccount.getBankAccountId(), target.getBankAccountId());
        assertEquals(bankAccount.getBalance(), target.getBalance());
        assertEquals(bankAccount.getMinimumBalance(), target.getMinimumBalance());
        assertEquals(bankAccount.getDescription(), target.getDescription());
        // attributes that should be excluded
        assertNull(target.getAccountHolderId());
    }


    /*
        Test whether the right attributes are converted
        from Dto List to Entity List
     */
    @Test
    public void bankAccountDtoToEntityList() {
        List<BankAccount> target = bankAccountMapper.toEntity(bankAccountDtos);
        // assert that the created list isn't empty
        assertFalse(target.isEmpty());
        // attributes that should be included
        assertEquals(bankAccountDto.getDescription(), target.get(0).getDescription());
        assertEquals(bankAccountDto.getAccountHolderId(), target.get(0).getAccountHolder().getUserId());
        // attributes that should be excluded
        assertEquals(bankAccountDto.getBankAccountId(), target.get(0).getBankAccountId());
        assertEquals(bankAccountDto.getMinimumBalance(), target.get(0).getMinimumBalance());
        assertEquals(bankAccountDto.getBalance(), target.get(0).getBalance());
    }

    /*
        Test whether the right attributes are converted
        from Entity List to Dto List
     */
    @Test
    public void bankAccountEntityToDtoList() {
        List<BankAccountDto> target = bankAccountMapper.toDto(bankAccounts);
        // assert that BankAccountDto list has been initialized
        assertNotNull(target);
        // attributes that should be included
        assertEquals(bankAccount.getBankAccountId(), target.get(0).getBankAccountId());
        assertEquals(bankAccount.getAccountHolder().getUserId(), target.get(0).getAccountHolderId());
        assertEquals(bankAccount.getDescription(), target.get(0).getDescription());
        // attributes that should be excluded
        assertNull(target.get(0).getBalance());
        assertNull(target.get(0).getMinimumBalance());
    }

    /*
        Test whether the right attributes are converted
        from Entity List to private Dto List
     */
    @Test
    public void bankAccountEntityToPrivateDtoList() {
        List<BankAccountDto> target = bankAccountMapper.toPrivateDto(bankAccounts);
        // attributes that should be included
        assertEquals(bankAccount.getBankAccountId(), target.get(0).getBankAccountId());
        assertEquals(bankAccount.getAccountHolder().getUserId(), target.get(0).getAccountHolderId());
        assertEquals(bankAccount.getBalance(), target.get(0).getBalance());
        assertEquals(bankAccount.getMinimumBalance(), target.get(0).getMinimumBalance());
        assertEquals(bankAccount.getDescription(), target.get(0).getDescription());
    }


    /*
        Testing empty dto
     */
    @Test
    public void bankAccountDtoToEmptyEntity() {
        BankAccount target = bankAccountMapper.toEntity((BankAccountDto) null);
        // expect a null to be returned
        assertNull(target);
    }

    /*
        Testing empty entity
     */
    @Test
    public void bankAccountEntityToEmptyDto() {
        BankAccountDto target = bankAccountMapper.toDto((BankAccount) null);
        // expect a null to be returned
        assertNull(target);
    }

    /*
        Testing empty entity
     */
    @Test
    public void bankAccountEntityToEmptyPrivateDto() {
        BankAccountDto target = bankAccountMapper.toPrivateDto((BankAccount) null);
        // expect a null to be returned
        assertNull(target);
    }

    /*
        Testing empty dto list
     */
    @Test
    public void bankAccountDtoToEmptyEntityList() {
        List<BankAccount> target = bankAccountMapper.toEntity((List<BankAccountDto>) null);
        // expect a null to be returned
        assertNull(target);
    }

    /*
        Testing empty entity list
     */
    @Test
    public void bankAccountEntityToEmptyDtoList() {
        List<BankAccountDto> target = bankAccountMapper.toDto((List<BankAccount>) null);
        // expect a null to be returned
        assertNull(target);
    }

    /*
        Testing empty entity list again
     */
    @Test
    public void bankAccountEntityToEmptyPrivateDtoList() {
        List<BankAccountDto> target =  bankAccountMapper.toPrivateDto((List<BankAccount>) null);
        // expect a null to be returned
        assertNull(target);
    }
}
