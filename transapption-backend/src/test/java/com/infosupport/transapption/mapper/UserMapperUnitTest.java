package com.infosupport.transapption.mapper;

import com.infosupport.transapption.model.dto.UserDto;
import com.infosupport.transapption.model.entity.User;
import org.junit.Before;
import org.junit.Test;
import org.mapstruct.factory.Mappers;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class UserMapperUnitTest {

    private User user;
    private UserDto userDto;

    private List<User> users;
    private List<UserDto> userDtos;

    private final UserMapper userMapper = Mappers.getMapper(UserMapper.class);

    @Before
    public void init() {
        user = new User(1L, "username123", "Password123!", "ROLE_USER");
        userDto = new UserDto("username123", "Password123!", "Password123!");

        users = new ArrayList<>();
        userDtos = new ArrayList<>();

        users.add(user);
        userDtos.add(userDto);
    }

    /*
        Test whether the right attributes are converted
        from Dto to Entity
     */
    @Test
    public void userDtoToEntityHappyPath() {
        User target = userMapper.toEntity(userDto);
        // results
        assertNotNull(target);
        // attributes that should be included
        assertEquals(userDto.getUsername(), target.getUsername());
        assertEquals(userDto.getPassword(), target.getPassword());
        // attributes that should be excluded
        assertNull(target.getUserId());
    }

    /*
        Test whether the right attributes are converted
        from Dto to Entity
     */
    @Test
    public void userEntityToDtoHappyPath() {
        UserDto target = userMapper.toDto(user);
        // results
        assertNotNull(target);
        // attributes that should be included
        assertEquals(user.getUserId(), target.getUserId());
        assertEquals(user.getUsername(), target.getUsername());
        assertEquals(user.getRole(), target.getRole());
        // attributes that should be excluded
        assertNull(target.getPassword());
        assertNull(target.getConfirmPassword());
    }

    /*
        Add UserDto to an iterable
     */
    @Test
    public void userDtoToEntityList() {
        List<User> target = userMapper.toEntity(userDtos);
        // assert that the mapping went successfully
        assertFalse(target.isEmpty());
        // attributes that should be included
        assertEquals(userDto.getUsername(), target.get(0).getUsername());
        assertEquals(userDto.getPassword(), target.get(0).getPassword());
        // remove from list
        target.remove(0);
        // make sure its empty
        assertTrue(target.isEmpty());
    }

    /*
        Add User entity to list,
        test whether the entity was included in a Dto list
     */
    @Test
    public void userEntityToDtoList() {
        List<UserDto> target = userMapper.toDto(users);
        // make sure its empty
        assertFalse(target.isEmpty());
        // attributes that should be included
        assertEquals(user.getUserId(), target.get(0).getUserId());
        assertEquals(user.getUsername(), target.get(0).getUsername());
        assertEquals(user.getRole(), target.get(0).getRole());
        // attributes that should be excluded
        assertNull(target.get(0).getPassword());
        assertNull(target.get(0).getConfirmPassword());
        // remove from list
        target.remove(0);
        // make sure its empty
        assertTrue(target.isEmpty());
    }

    /*
        Gives empty user entity, returns null
     */
    @Test
    public void userDtoToNull() {
        User target = userMapper.toEntity((UserDto) null);
        // assert that the method returned 'null'
        assertNull(target);
    }

    /*
        Gives empty user DTO, returns null
     */
    @Test
    public void userToNull() {
        UserDto target = userMapper.toDto((User) null);
        // assert that the method returned 'null'
        assertNull(target);
    }

    /*
        Gives empty list, returns empty list
     */
    @Test
    public void userDtoToEmptyEntityList() {
        List<User> target = userMapper.toEntity((List<UserDto>) null);
        // results
        assertNull(target);
    }

    /*
        Gives empty list, returns empty list
     */
    @Test
    public void userEntityToEmptyDtoList() {
        List<UserDto> target = userMapper.toDto((List<User>) null);
        // results
        assertNull(target);
    }
}