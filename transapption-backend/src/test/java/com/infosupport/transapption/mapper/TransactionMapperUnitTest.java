package com.infosupport.transapption.mapper;

import com.infosupport.transapption.model.dto.TransactionDto;
import com.infosupport.transapption.model.entity.BankAccount;
import com.infosupport.transapption.model.entity.Transaction;
import com.infosupport.transapption.model.entity.User;
import com.infosupport.transapption.service.BankAccountService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class TransactionMapperUnitTest {

    private Transaction transaction;
    private TransactionDto transactionDto;
    private List<Transaction> transactions;
    private List<TransactionDto> transactionDtos;

    @Mock
    private BankAccountService bankAccountService;

    @InjectMocks
    private final TransactionMapper transactionMapper = Mappers.getMapper(TransactionMapper.class);

    @Before
    public void init() {
        // init the account holders
        final User user1 = new User(1L, "Username1", "Password123!", "ROLE_USER");
        final User user2 = new User(2L, "Username2", "Password123!", "ROLE_USER");
        // init the bank accounts of the account holders
        final BankAccount sender = new BankAccount(1L, user1, 100L, 0L, "description");
        final BankAccount receiver = new BankAccount(2L, user2, 100L, 0L, "description");

        transaction = new Transaction(1L, sender, receiver, 10, "01-02-2018 23:59:59", "transaction description");
        transactionDto = new TransactionDto(1L, 2L, 100, "transactionDto description");

        transactions = new ArrayList<>();
        transactionDtos = new ArrayList<>();

        transactions.add(transaction);
        transactionDtos.add(transactionDto);

        Mockito.when(bankAccountService.getBankAccountById(1L))
                .thenReturn(sender);
        Mockito.when(bankAccountService.getBankAccountById(2L))
                .thenReturn(receiver);
    }

    /*
        Test whether the right attributes are converted
        from Dto to Entity
     */
    @Test
    public void transactionDtoToEntityHappyPath() {
        Transaction target = transactionMapper.toEntity(transactionDto);
        // assert that the transaction has been initialized
        assertNotNull(target);
        // attributes that should be included
        assertEquals(transactionDto.getSenderId(), target.getSender().getBankAccountId());
        assertEquals(transactionDto.getReceiverId(), target.getReceiver().getBankAccountId());
        assertEquals(transactionDto.getAmount(), target.getAmount());
        assertEquals(transactionDto.getDescription(), target.getDescription());
        // attributes that should be excluded
        assertNull(target.getTransactionId());
    }

    /*
        Test whether the right attributes are converted
        from Dto to Entity
     */
    @Test
    public void transactionEntityToDtoHappyPath() {
        TransactionDto target = transactionMapper.toDto(transaction);
        // assert that the transaction has been initialized
        assertNotNull(target);
        // attributes that should be included
        assertEquals(transaction.getTransactionId(), target.getTransactionId());
        assertEquals(transaction.getSender().getBankAccountId(), target.getSenderId());
        assertEquals(transaction.getReceiver().getBankAccountId(), target.getReceiverId());
        assertEquals(transaction.getDescription(), target.getDescription());
        assertEquals(transaction.getTimestamp(), target.getTimestamp());
        assertEquals(transaction.getAmount(), target.getAmount());
    }

    @Test
    public void transactionDtoListToEntity() {
        List<Transaction> target = transactionMapper.toEntity(transactionDtos);
        // attributes that should be included
        assertEquals(transactionDto.getSenderId(), target.get(0).getSender().getBankAccountId());
        assertEquals(transactionDto.getReceiverId(), target.get(0).getReceiver().getBankAccountId());
        assertEquals(transactionDto.getAmount(), target.get(0).getAmount());
        assertEquals(transactionDto.getDescription(), target.get(0).getDescription());
        // attributes that should be excluded
        assertNull(target.get(0).getTransactionId());
        assertNull(target.get(0).getTimestamp());
        // remove the entry
        target.remove(0);
        // make sure its empty
        assertTrue(target.isEmpty());
    }

    @Test
    public void transactionEntityListToDto() {
        List<TransactionDto> target = transactionMapper.toDto(transactions);
        // attributes that should be included
        assertEquals(transaction.getTransactionId(), target.get(0).getTransactionId());
        assertEquals(transaction.getSender().getBankAccountId(), target.get(0).getSenderId());
        assertEquals(transaction.getReceiver().getBankAccountId(), target.get(0).getReceiverId());
        assertEquals(transaction.getDescription(), target.get(0).getDescription());
        assertEquals(transaction.getTimestamp(), target.get(0).getTimestamp());
        assertEquals(transaction.getAmount(), target.get(0).getAmount());
        // remove the entry
        target.remove(0);
        // make sure its empty
        assertTrue(target.isEmpty());
    }

    /*
        Test whether the sender and receiver id's are set to null
        when passing dtos with no bank account id
     */
    @Test
    public void transactionEntityToDtoNoSenderAndReceiverId() {
        // remove bank account id's
        transaction.getSender().setBankAccountId(null);
        transaction.getReceiver().setBankAccountId(null);
        // run the actual method
        TransactionDto target = transactionMapper.toDto(transaction);
        // important attributes
        assertNull(target.getSenderId());
        assertNull(target.getReceiverId());
        // verify other attributes
        assertEquals(transaction.getTransactionId(), target.getTransactionId());
        assertEquals(transaction.getDescription(), target.getDescription());
        assertEquals(transaction.getTimestamp(), target.getTimestamp());
        assertEquals(transaction.getAmount(), target.getAmount());
    }

    /*
        Test whether the sender and receiver id's are set to null
        when passing dtos without a sender and receiver
     */
    @Test
    public void transactionEntityToDtoNoSenderAndReceiver() {
        // remove bank account id's
        transaction.setSender(null);
        transaction.setReceiver(null);
        // run the actual method
        TransactionDto target = transactionMapper.toDto(transaction);
        // important attributes
        assertNull(target.getSenderId());
        assertNull(target.getReceiverId());
        // verify other attributes
        assertEquals(transaction.getTransactionId(), target.getTransactionId());
        assertEquals(transaction.getDescription(), target.getDescription());
        assertEquals(transaction.getTimestamp(), target.getTimestamp());
        assertEquals(transaction.getAmount(), target.getAmount());
    }

    /*
        Assert that an empty dto returns 'null'
     */
    @Test
    public void transactionDtoToEmpty() {
        Transaction target = transactionMapper.toEntity((TransactionDto) null);
        // assert that it returned null
        assertNull(target);
    }

    /*
        Assert that an empty entity returns 'null'
     */
    @Test
    public void transactionEntityToEmpty() {
        TransactionDto target = transactionMapper.toDto((Transaction) null);
        // assert that it returned null
        assertNull(target);
    }

    /*
        Assert that an empty dto list returns 'null'
     */
    @Test
    public void transactionDtoListToEmpty() {
        List<Transaction> target = transactionMapper.toEntity((List<TransactionDto>) null);
        // assert that it returned null
        assertNull(target);
    }

    /*
        Assert that an empty entity list returns 'null'
     */
    @Test
    public void transactionEntityToList() {
        List<TransactionDto> target = transactionMapper.toDto((List<Transaction>) null);
        // assert that it returned null
        assertNull(target);
    }
}
