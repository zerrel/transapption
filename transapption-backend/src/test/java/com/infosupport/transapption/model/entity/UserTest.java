package com.infosupport.transapption.model.entity;

import org.junit.Test;

import static org.junit.Assert.*;

public class UserTest {

    @Test
    public void testEquals() {
        User user1 = new User(1L, "username", "password", "ROLE_USER");
        User user2 = new User(1L, "username", "password", "ROLE_USER");
        assertTrue(user1.equals(user2) && user2.equals(user1));
        assertEquals(user1.hashCode(), user2.hashCode());
    }

    @Test
    public void testNotEquals() {
        User user1 = new User(1L, "username", "password", "ROLE_USER");
        User user2 = new User(2L, "username", "password", "ROLE_USER");
        assertFalse(user1.equals(user2) && user2.equals(user1));
        assertNotEquals(user1.hashCode(), user2.hashCode());
    }

}