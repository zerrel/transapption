package com.infosupport.transapption.model.entity;

import org.junit.Test;

import static org.junit.Assert.*;

public class TransactionTest {

    @Test
    public void testEquals() {
        BankAccount bankAccount = new BankAccount();
        Transaction transaction1 = new Transaction(1L, bankAccount, bankAccount, 100, "31-12-2011 11:11:11", "description");
        Transaction transaction2 = new Transaction(1L, bankAccount, bankAccount, 100, "31-12-2011 11:11:11", "description");
        assertTrue(transaction1.equals(transaction2) && transaction2.equals(transaction1));
        assertEquals(transaction1.hashCode(), transaction2.hashCode());
    }

    @Test
    public void testNotEquals() {
        BankAccount bankAccount = new BankAccount();
        Transaction transaction1 = new Transaction(1L, bankAccount, bankAccount, 200, "31-12-2011 11:11:11", "description");
        Transaction transaction2 = new Transaction(2L, bankAccount, bankAccount, 200, "31-12-2011 11:11:11", "description");
        assertFalse(transaction1.equals(transaction2) || transaction2.equals(transaction1));
        assertNotEquals(transaction1.hashCode(), transaction2.hashCode());
    }

}