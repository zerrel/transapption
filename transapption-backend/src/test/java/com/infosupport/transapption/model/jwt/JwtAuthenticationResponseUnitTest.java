package com.infosupport.transapption.model.jwt;

import org.junit.Test;

import static org.junit.Assert.*;

public class JwtAuthenticationResponseUnitTest {

    private final String token = "token";

    @Test
    public void authenticationResponseEmptyConstructor() {
        JwtAuthenticationResponse authenticationResponse = new JwtAuthenticationResponse();
        assertNull(authenticationResponse.getToken());
    }

    @Test
    public void authenticationResponseConstructor() {
        JwtAuthenticationResponse authenticationResponse = new JwtAuthenticationResponse(token);
        assertEquals(token, authenticationResponse.getToken());
    }

    @Test
    public void authenticationResponseSetters() {
        JwtAuthenticationResponse authenticationResponse = new JwtAuthenticationResponse();
        authenticationResponse.setToken(token);
        assertEquals(token, authenticationResponse.getToken());
    }

}