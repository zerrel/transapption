package com.infosupport.transapption.model.dto;

import org.hibernate.validator.HibernateValidator;
import org.junit.Before;
import org.junit.Test;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.validation.ConstraintViolation;
import java.util.Collections;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/*
    Testing hibernate validation annotations for Transaction
 */
public class TransactionDtoValidationTest {

    private LocalValidatorFactoryBean localValidatorFactory;

    @Before
    public void setup() {
        localValidatorFactory = new LocalValidatorFactoryBean();
        localValidatorFactory.setProviderClass(HibernateValidator.class);
        localValidatorFactory.afterPropertiesSet();
    }

    @Test
    public void testPositiveAmount() {
        TransactionDto transaction = new TransactionDto(1L, 2L, -1, "some description");
        Set<ConstraintViolation<TransactionDto>> violations = localValidatorFactory.validate(transaction);
        assertEquals(1, violations.size());
        assertEquals("amount", violations.iterator().next().getPropertyPath().toString());
        assertEquals("The amount of the Transaction must be a positive value.",
                violations.iterator().next().getMessage());
    }

    @Test
    public void testSizeDescription() {
        TransactionDto transaction = new TransactionDto(1L, 2L, 10, "");
        transaction.setDescription(String.join("", Collections.nCopies(101, "a")));
        Set<ConstraintViolation<TransactionDto>> violations = localValidatorFactory.validate(transaction);
        assertEquals(1, violations.size());
        assertEquals("description", violations.iterator().next().getPropertyPath().toString());
        assertEquals("The description of the transaction should contain less than 100 characters.",
                violations.iterator().next().getMessage());
    }

    @Test
    public void testNoSenderAndReceiver() {
        TransactionDto transaction = new TransactionDto(null, null, 10, "description");
        Set<ConstraintViolation<TransactionDto>> violations = localValidatorFactory.validate(transaction);
        assertEquals(2, violations.size());
        assertEquals(2, violations.stream().filter(
                m -> m.getMessage().equals("must not be null")).count());
    }

}