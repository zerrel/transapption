package com.infosupport.transapption.model.entity;

import org.junit.Test;

import static org.junit.Assert.*;

public class BankAccountTest {

    @Test
    public void testEquals() {
        User accountHolder = new User();
        BankAccount bankAccount1 = new BankAccount(accountHolder, 100L, 0L, "description");
        BankAccount bankAccount2 = new BankAccount(accountHolder, 100L, 0L, "description");
        assertTrue(bankAccount1.equals(bankAccount2) && bankAccount2.equals(bankAccount1));
        assertEquals(bankAccount1.hashCode(), bankAccount2.hashCode());
    }

    @Test
    public void testNotEquals() {
        User accountHolder = new User();
        BankAccount bankAccount1 = new BankAccount(accountHolder, 100L, 0L, "description");
        BankAccount bankAccount2 = new BankAccount(accountHolder, 100L, 10L, "description");
        assertFalse(bankAccount1.equals(bankAccount2) || bankAccount2.equals(bankAccount1));
        assertNotEquals(bankAccount1.hashCode(), bankAccount2.hashCode());
    }

}