package com.infosupport.transapption.model.dto;

import org.hibernate.validator.HibernateValidator;
import org.junit.Before;
import org.junit.Test;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.validation.ConstraintViolation;
import java.util.Collections;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/*
    Testing hibernate validation annotations for Bank Account
 */
public class BankAccountDtoValidationTest {

    private LocalValidatorFactoryBean localValidatorFactory;

    @Before
    public void setup() {
        localValidatorFactory = new LocalValidatorFactoryBean();
        localValidatorFactory.setProviderClass(HibernateValidator.class);
        localValidatorFactory.afterPropertiesSet();
    }

    @Test
    public void testMaxLengthDescription() {
        String description = String.join("", Collections.nCopies(101, "a"));
        BankAccountDto bankAccount = new BankAccountDto(1L, 100L, 0L, description);
        Set<ConstraintViolation<BankAccountDto>> violations = localValidatorFactory.validate(bankAccount);
        assertEquals(1, violations.size());
        assertEquals("description", violations.iterator().next().getPropertyPath().toString());
        assertEquals("The description of the bank account should contain less than 100 characters.",
                violations.iterator().next().getMessage());
    }

    @Test
    public void testNotNullAttributes() {
        BankAccountDto bankAccount = new BankAccountDto();
        Set<ConstraintViolation<BankAccountDto>> violations = localValidatorFactory.validate(bankAccount);
        assertEquals(3, violations.size());
        assertEquals(3, violations.stream().filter(
                m -> m.getMessage().equals("must not be null")).count());
    }

}