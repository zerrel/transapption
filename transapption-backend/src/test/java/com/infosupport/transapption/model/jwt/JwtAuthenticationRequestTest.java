package com.infosupport.transapption.model.jwt;

import org.junit.Test;

import static org.junit.Assert.*;

public class JwtAuthenticationRequestTest {

    private final String username = "username";
    private final String password = "password";

    @Test
    public void authenticationRequestEmptyConstructor() {
        JwtAuthenticationRequest authenticationRequest =
                new JwtAuthenticationRequest();
        assertNull(authenticationRequest.getUsername());
        assertNull(authenticationRequest.getPassword());
    }

    @Test
    public void authenticationRequestConstructor() {
        JwtAuthenticationRequest authenticationRequest =
                new JwtAuthenticationRequest(username, password);
        assertEquals(username, authenticationRequest.getUsername());
        assertEquals(password, authenticationRequest.getPassword());
    }

    @Test
    public void authenticationRequestSetters() {
        JwtAuthenticationRequest authenticationRequest =
                new JwtAuthenticationRequest();
        authenticationRequest.setUsername(username);
        authenticationRequest.setPassword(password);
        assertEquals(username, authenticationRequest.getUsername());
        assertEquals(password, authenticationRequest.getPassword());
    }

}