package com.infosupport.transapption.model.dto;

import org.hibernate.validator.HibernateValidator;
import org.junit.Before;
import org.junit.Test;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/*
    Testing hibernate validation annotations for User
 */
public class UserDtoValidationTest {

    private LocalValidatorFactoryBean localValidatorFactory;

    @Before
    public void setup() {
        localValidatorFactory = new LocalValidatorFactoryBean();
        localValidatorFactory.setProviderClass(HibernateValidator.class);
        localValidatorFactory.afterPropertiesSet();
    }

    @Test
    public void testBlankFields() {
        UserDto user = new UserDto();
        Set<ConstraintViolation<UserDto>> violations = localValidatorFactory.validate(user);
        assertEquals(3, violations.size());
        assertEquals(3, violations.stream().filter(
                m -> m.getMessage().equals("must not be blank")).count());
    }

    @Test
    public void testSpecialCharactersUsername() {
        UserDto user = new UserDto("*******", "Password123!", "Password123!");
        Set<ConstraintViolation<UserDto>> violations = localValidatorFactory.validate(user);
        assertEquals("username", violations.iterator().next().getPropertyPath().toString());
        assertEquals("The username cannot contain any special or whitespace characters.",
                violations.iterator().next().getMessage());
    }

    @Test
    public void testSizeUsername() {
        UserDto user = new UserDto("user", "Password123!", "Password123!");
        Set<ConstraintViolation<UserDto>> violations = localValidatorFactory.validate(user);
        assertEquals("username", violations.iterator().next().getPropertyPath().toString());
        assertEquals("The username must be between 5 and 20 characters long.",
                violations.iterator().next().getMessage());
    }

    @Test
    public void testSizePassword() {
        UserDto user = new UserDto("username", "pass", "pass");
        Set<ConstraintViolation<UserDto>> violations = localValidatorFactory.validate(user);
        assertEquals("password", violations.iterator().next().getPropertyPath().toString());
        assertEquals("The password must be between 8 and 30 characters long.",
                violations.iterator().next().getMessage());
    }

}