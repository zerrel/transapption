package com.infosupport.transapption.repository;

import com.infosupport.transapption.model.entity.BankAccount;
import com.infosupport.transapption.model.entity.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class BankAccountRepositoryIntTest {

    private User user;
    private BankAccount bankAccount;

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Before
    public void init() {
        String bcryptPassword = "$2a$10$pnBUWJw.u2IO2MhU1LYIhuCVHmXYJtqK1Zbwi7m22NqCGLbirMA5W";
        user = new User("Username789", bcryptPassword, "USER_ROLE");
        bankAccount = new BankAccount(user, 100L, 0L, "description bank account");
    }

    @After
    public void tearDown() {
        bankAccountRepository.deleteAll();
    }

    /*
        Adding a BankAccount with AccountHolder to the database
     */
    @Test
    public void addBankAccountToDatabase() throws Exception {
        entityManager.persist(bankAccount);
        BankAccount newBankAccount = bankAccountRepository.findById(bankAccount.getBankAccountId())
                                        .orElseThrow(Exception::new);
        assertEquals(user, newBankAccount.getAccountHolder());
    }

    /*
        Updating the description of a BankAccount
     */
    @Test
    public void updateBankAccountDescription() throws Exception {
        BankAccount savedBankAccount = entityManager.persistFlushFind(bankAccount);
        savedBankAccount.setDescription("something else");
        entityManager.persist(savedBankAccount);
        BankAccount updatedBankAccount = bankAccountRepository.findById(savedBankAccount.getBankAccountId())
                                            .orElseThrow(Exception::new);
        assertEquals("something else", updatedBankAccount.getDescription());
    }

}