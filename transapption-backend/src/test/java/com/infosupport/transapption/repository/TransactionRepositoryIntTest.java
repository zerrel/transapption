package com.infosupport.transapption.repository;

import com.infosupport.transapption.model.entity.BankAccount;
import com.infosupport.transapption.model.entity.Transaction;
import com.infosupport.transapption.model.entity.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TransactionRepositoryIntTest {

    private User user1;
    private User user2;
    private BankAccount sender;
    private BankAccount receiver;
    private Transaction transaction;

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TransactionRepository transactionRepository;

    @Before
    public void init() {
        String bcryptPassword = "$2a$10$pnBUWJw.u2IO2MhU1LYIhuCVHmXYJtqK1Zbwi7m22NqCGLbirMA5W";
        user1 = new User("Username234", bcryptPassword, "USER_ROLE");
        user2 = new User("Username456", bcryptPassword, "USER_ROLE");
        sender = new BankAccount(user1, 100L, 0L, "description sender bank account");
        receiver = new BankAccount(user2, 100L, 0L, "description receiver bank account");
        transaction = new Transaction(sender, receiver, 10, "01-02-2018 23:59:59", "transaction description");
    }

    @After
    public void tearDown() {
        transactionRepository.deleteAll();
    }

    @Test
    public void addTransactionToDatabase() throws Exception {
        entityManager.persist(transaction);
        Transaction newTransaction = transactionRepository.findById(transaction.getTransactionId())
                .orElseThrow(Exception::new);
        // Compare the results with the expected results
        assertEquals(transaction.getTransactionId(), newTransaction.getTransactionId());
        assertEquals(transaction.getSender().getBankAccountId(), newTransaction.getSender().getBankAccountId());
        assertEquals(transaction.getReceiver().getBankAccountId(), newTransaction.getReceiver().getBankAccountId());
        assertEquals(transaction.getSender().getAccountHolder().getPassword(), newTransaction.getSender().getAccountHolder().getPassword());
        assertEquals(transaction.getReceiver().getAccountHolder().getPassword(), newTransaction.getReceiver().getAccountHolder().getPassword());
    }

}
