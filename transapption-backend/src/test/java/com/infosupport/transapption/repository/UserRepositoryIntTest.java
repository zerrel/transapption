package com.infosupport.transapption.repository;

import com.infosupport.transapption.model.entity.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryIntTest {

    private User user;

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @Before
    public void init() {
        user = new User("Username123", "Password123!", "ROLE_USER");
        entityManager.persist(user);
    }

    @After
    public void tearDown() {
        userRepository.deleteAll();
    }

    /*
        Test if user can be added to the database
     */
    @Test
    public void addUserToDatabase() throws Exception {
        User newUser = userRepository.findByUsername(user.getUsername())
            .orElseThrow(Exception::new);
        assertEquals(user.getUsername(), newUser.getUsername());
        assertEquals(user.getPassword(), newUser.getPassword());
    }

    /*
        Test if user can be removed
     */
    @Test
    public void deleteUser() throws Exception {
        User newUser = userRepository.findByUsername(user.getUsername())
                .orElseThrow(Exception::new);
        userRepository.deleteById(newUser.getUserId());
        assertEquals(userRepository.findByUsername(user.getUsername()), Optional.empty());
    }

    /*
        Test if the password of a user can be updated
     */
    @Test
    public void updatePassword() throws Exception {
        User newUser = userRepository.findByUsername(user.getUsername())
                .orElseThrow(Exception::new);
        newUser.setPassword("password2");
        entityManager.persist(newUser);
        Optional<User> updatedUser = userRepository.findByUsername(user.getUsername());
        User newUpdatedUser = updatedUser.orElseThrow(Exception::new);
        assertEquals("password2", newUpdatedUser.getPassword());
    }

}