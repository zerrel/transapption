package com.infosupport.transapption.component.validation;

import com.infosupport.transapption.component.UserPrincipalExtractor;
import com.infosupport.transapption.model.dto.TransactionDto;
import com.infosupport.transapption.model.entity.BankAccount;
import com.infosupport.transapption.model.entity.User;
import com.infosupport.transapption.service.BankAccountService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TransactionDtoValidatorTest {

    @Mock
    private BankAccountService bankAccountService;

    @Mock
    private UserPrincipalExtractor userPrincipalExtractor;

    @InjectMocks
    private TransactionDtoValidator transactionDtoValidator;

    @Test
    public void testSenderAndReceiverDoNotExist() {
        when(bankAccountService.getBankAccountById(any(Long.class))).thenReturn(null);
        TransactionDto transaction = new TransactionDto(1L, 2L, 100, "some transaction");
        Errors errors = new BeanPropertyBindingResult(transaction, "");
        transactionDtoValidator.validate(transaction, errors);
        assertEquals(2, errors.getErrorCount());
        assertEquals("The Bank Account of the sender does not exist.",
                errors.getFieldError("senderId").getDefaultMessage());
        assertEquals("The Bank Account of the receiver does not exist.",
                errors.getFieldError("receiverId").getDefaultMessage());
    }

    @Test
    public void testUserIsNotOwner() {
        User currentUser = new User(1L, "u", "p", "r");
        User user = new User(2L, "u2", "p", "r");
        BankAccount bankAccount = new BankAccount(5L, user, 100L, 0L, "description");
        when(bankAccountService.getBankAccountById(any(Long.class))).thenReturn(bankAccount);
        when(userPrincipalExtractor.getCurrentUser()).thenReturn(currentUser);
        TransactionDto transaction = new TransactionDto(5L, 6L, 10, "some transaction");
        Errors errors = new BeanPropertyBindingResult(transaction, "");
        transactionDtoValidator.validate(transaction, errors);
        assertEquals(1, errors.getErrorCount());
        assertEquals("The logged in User is not the owner of this BankAccount.",
                errors.getFieldError("senderId").getDefaultMessage());
    }

    @Test
    public void testTransactionAmountExceedsMinimumBalance() {
        User user = new User(2L, "u2", "p", "r");
        BankAccount bankAccount = new BankAccount(5L, user, 100L, 0L, "description");
        when(bankAccountService.getBankAccountById(any(Long.class))).thenReturn(bankAccount);
        when(userPrincipalExtractor.getCurrentUser()).thenReturn(user);
        TransactionDto transaction = new TransactionDto(5L, 6L, 1000, "some transaction");
        Errors errors = new BeanPropertyBindingResult(transaction, "");
        transactionDtoValidator.validate(transaction, errors);
        assertEquals(1, errors.getErrorCount());
        assertEquals("The sender has insufficient credits to perform this Transaction.",
                errors.getFieldError("amount").getDefaultMessage());
    }

    @Test
    public void testTransactionAmountExceedsMaximumBalance() {
        User user = new User(2L, "u2", "p", "r");
        BankAccount sender = new BankAccount(5L, user, 100L, 0L, "description");
        BankAccount receiver = new BankAccount(6L, user, Long.MAX_VALUE, 0L, "description");
        when(bankAccountService.getBankAccountById(5L)).thenReturn(sender);
        when(bankAccountService.getBankAccountById(6L)).thenReturn(receiver);
        when(userPrincipalExtractor.getCurrentUser()).thenReturn(user);
        TransactionDto transaction = new TransactionDto(5L, 6L, 100, "transaction");
        Errors errors = new BeanPropertyBindingResult(transaction, "");
        transactionDtoValidator.validate(transaction, errors);
        assertEquals(1, errors.getErrorCount());
        assertEquals("The Bank Account of the receiver has a too high balance.",
                errors.getFieldError("amount").getDefaultMessage());
    }

    @Test
    public void testSenderAndReceiverSame() {
        User user = new User(2L, "u2", "p", "r");
        BankAccount bankAccount = new BankAccount(5L, user, 100L, 0L, "description");
        when(bankAccountService.getBankAccountById(any(Long.class))).thenReturn(bankAccount);
        when(userPrincipalExtractor.getCurrentUser()).thenReturn(user);
        TransactionDto transaction = new TransactionDto(5L, 5L, 10, "transaction");
        Errors errors = new BeanPropertyBindingResult(transaction, "");
        transactionDtoValidator.validate(transaction, errors);
        assertEquals(1, errors.getErrorCount());
        assertEquals("The sender and receiver cannot be the same.",
                errors.getFieldError("receiverId").getDefaultMessage());
    }

    @Test
    public void testUserHappyPath() {
        User user = new User(2L, "u2", "p", "r");
        BankAccount sender = new BankAccount(5L, user, 100L, 0L, "description");
        BankAccount receiver = new BankAccount(6L, user, 100L, 0L, "description");
        when(bankAccountService.getBankAccountById(5L)).thenReturn(sender);
        when(bankAccountService.getBankAccountById(6L)).thenReturn(receiver);
        when(userPrincipalExtractor.getCurrentUser()).thenReturn(user);
        TransactionDto transaction = new TransactionDto(5L, 6L, 10, "transaction");
        Errors errors = new BeanPropertyBindingResult(transaction, "");
        transactionDtoValidator.validate(transaction, errors);
        assertEquals(0, errors.getErrorCount());
    }

    @Test
    public void testAdminHappyPath() {
        User admin = new User(2L, "u2", "p", "ROLE_ADMIN");
        User user = new User(2L, "u2", "p", "r");
        BankAccount sender = new BankAccount(5L, user, 100L, 0L, "description");
        BankAccount receiver = new BankAccount(6L, user, 100L, 0L, "description");
        when(bankAccountService.getBankAccountById(5L)).thenReturn(sender);
        when(bankAccountService.getBankAccountById(6L)).thenReturn(receiver);
        when(userPrincipalExtractor.getCurrentUser()).thenReturn(admin);
        TransactionDto transaction = new TransactionDto(5L, 6L, 10, "transaction");
        Errors errors = new BeanPropertyBindingResult(transaction, "");
        transactionDtoValidator.validate(transaction, errors);
        assertEquals(0, errors.getErrorCount());
    }

    @Test
    public void testEmptyFields() {
        TransactionDto transactionDto = new TransactionDto();
        Errors errors = new BeanPropertyBindingResult(transactionDto, "");
        transactionDtoValidator.validate(transactionDto, errors);
        assertEquals(0, errors.getErrorCount());
    }

    @Test
    public void testSupport() {
        assertTrue(transactionDtoValidator.supports(TransactionDto.class));
    }

    @Test
    public void testWrongSupport() {
        assertFalse(transactionDtoValidator.supports(String.class));
    }
}