package com.infosupport.transapption.component;

import com.infosupport.transapption.model.entity.User;
import com.infosupport.transapption.repository.UserRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserPrincipalExtractorTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private Authentication authentication;

    @Mock
    private AnonymousAuthenticationToken anonymousAuthenticationToken;

    @Mock
    private SecurityContext securityContext;

    @InjectMocks
    private UserPrincipalExtractor userPrincipalExtractor;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void retrieveLoggedInUser() {
        User user = new User(1L, "username", "password", "ROLE_USER");
        when(securityContext.getAuthentication())
                .thenReturn(authentication);
        when(authentication.getName())
                .thenReturn(user.getUsername());
        SecurityContextHolder.setContext(securityContext);
        when(userRepository.findByUsername(any(String.class)))
                .thenReturn(java.util.Optional.of(user));
        User currentUser = userPrincipalExtractor.getCurrentUser();
        // assertions
        assertNotNull(user);
        assertEquals(user.getUsername(), currentUser.getUsername());
        assertEquals(user.getUserId(), currentUser.getUserId());
        assertEquals(user.getRole(), currentUser.getRole());
    }

    @Test
    public void retrieveLoggedOutUser() {
        when(securityContext.getAuthentication())
                .thenReturn(anonymousAuthenticationToken);
        SecurityContextHolder.setContext(securityContext);
        exception.expect(AuthenticationCredentialsNotFoundException.class);
        userPrincipalExtractor.getCurrentUser();
    }

    @Test
    public void retrieveLoggedInUserDoesNotExist() {
        when(securityContext.getAuthentication())
                .thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        exception.expect(UsernameNotFoundException.class);
        userPrincipalExtractor.getCurrentUser();
    }
}