package com.infosupport.transapption.component.validation;

import com.infosupport.transapption.model.dto.UserDto;
import com.infosupport.transapption.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserDtoValidatorTest {

    @Mock
    private UserService userService;

    @InjectMocks
    private UserDtoValidator userDtoValidator;

    @Test
    public void testEveryFieldIsInvalid() {
        when(userService.userNotExists(any(String.class))).thenReturn(false);
        UserDto userDto = new UserDto("username", "password", "notpassword");
        Errors errors = new BeanPropertyBindingResult(userDto, "");
        userDtoValidator.validate(userDto, errors);
        assertEquals(3, errors.getErrorCount());
        assertEquals("This username is already in use.", errors.getFieldError("username").getDefaultMessage());
        assertEquals("The password you have chosen is too weak.", errors.getFieldErrors("password").get(0).getDefaultMessage());
        assertEquals("The passwords do not match.", errors.getFieldErrors("password").get(1).getDefaultMessage());
    }

    @Test
    public void testEveryFieldIsValid() {
        when(userService.userNotExists(any(String.class))).thenReturn(true);
        UserDto userDto = new UserDto("username", "Password123!", "Password123!");
        Errors errors = new BeanPropertyBindingResult(userDto, "");
        userDtoValidator.validate(userDto, errors);
        assertEquals(0, errors.getErrorCount());
    }

    @Test
    public void testEmptyFields() {
        UserDto userDto = new UserDto();
        Errors errors = new BeanPropertyBindingResult(userDto, "");
        userDtoValidator.validate(userDto, errors);
        assertEquals(0, errors.getErrorCount());
    }

    @Test
    public void testSupport() {
        assertTrue(userDtoValidator.supports(UserDto.class));
    }

    @Test
    public void testSupportWrongClass() {
        assertFalse(userDtoValidator.supports(String.class));
    }

}