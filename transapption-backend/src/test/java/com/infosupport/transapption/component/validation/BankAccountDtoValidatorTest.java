package com.infosupport.transapption.component.validation;

import com.infosupport.transapption.component.UserPrincipalExtractor;
import com.infosupport.transapption.model.dto.BankAccountDto;
import com.infosupport.transapption.model.entity.User;
import com.infosupport.transapption.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BankAccountDtoValidatorTest {

    private Errors errors;
    private BankAccountDto bankAccount;

    @Mock
    private UserPrincipalExtractor userPrincipalExtractor;

    @Mock
    private UserService userService;

    @InjectMocks
    private BankAccountDtoValidator bankAccountDtoValidator;

    @Before
    public void setUp() {
        when(userService.getUserById(any(Long.class))).thenReturn(new User());
        when(userPrincipalExtractor.getCurrentUser()).thenReturn(new User(1L, "u", "p", "r"));
        bankAccount = new BankAccountDto(1L, 0L, 0L, "some description");
    }

    @Test
    public void testAccountHolderIdDoesNotExist() {
        when(userService.getUserById(any(Long.class))).thenReturn(null);
        errors = new BeanPropertyBindingResult(bankAccount, "");
        bankAccountDtoValidator.validate(bankAccount, errors);
        assertEquals(1, errors.getErrorCount());
        assertEquals("There doesn't exist any User with this id.",
                errors.getFieldError("accountHolderId").getDefaultMessage());
    }

    @Test
    public void testWrongAccountHolderId() {
        bankAccount.setAccountHolderId(5L);
        errors = new BeanPropertyBindingResult(bankAccount, "");
        bankAccountDtoValidator.validate(bankAccount, errors);
        assertEquals(1, errors.getErrorCount());
        assertEquals("The logged in User is not the owner of the BankAccount.",
                errors.getFieldError("accountHolderId").getDefaultMessage());
    }

    @Test
    public void testWrongMinimumBalance() {
        bankAccount.setMinimumBalance(100L);
        errors = new BeanPropertyBindingResult(bankAccount, "");
        bankAccountDtoValidator.validate(bankAccount, errors);
        assertEquals(1, errors.getErrorCount());
        assertEquals("The minimumBalance has to be zero.",
                errors.getFieldError("minimumBalance").getDefaultMessage());
    }

    @Test
    public void testWrongBalance() {
        bankAccount.setBalance(10L);
        errors = new BeanPropertyBindingResult(bankAccount, "");
        bankAccountDtoValidator.validate(bankAccount, errors);
        assertEquals(1, errors.getErrorCount());
        assertEquals("The balance has to be zero",
                errors.getFieldError("balance").getDefaultMessage());
    }

    @Test
    public void testUserCreatesBankAccount() {
        errors = new BeanPropertyBindingResult(bankAccount, "");
        bankAccountDtoValidator.validate(bankAccount, errors);
        assertEquals(0, errors.getErrorCount());
    }

    /*
        Create a Bank Account with minimum balance other than 0,
        while logged in as Admin
     */
    @Test
    public void testAdminCreatesBankAccount() {
        // override mock behaviour
        when(userPrincipalExtractor.getCurrentUser()).thenReturn(new User(1L, "u", "p", "ROLE_ADMIN"));
        bankAccount.setMinimumBalance(-100L);
        errors = new BeanPropertyBindingResult(bankAccount, "");
        bankAccountDtoValidator.validate(bankAccount, errors);
        assertEquals(0, errors.getErrorCount());
    }

    @Test
    public void testEmptyFields() {
        BankAccountDto emptyBankAccountDto = new BankAccountDto();
        errors = new BeanPropertyBindingResult(bankAccount, "");
        bankAccountDtoValidator.validate(emptyBankAccountDto, errors);
        assertEquals(0, errors.getErrorCount());
    }

    @Test
    public void testSupport() {
        assertTrue(bankAccountDtoValidator.supports(BankAccountDto.class));
    }

    @Test
    public void testWrongSupport() {
        assertFalse(bankAccountDtoValidator.supports(String.class));
    }

}