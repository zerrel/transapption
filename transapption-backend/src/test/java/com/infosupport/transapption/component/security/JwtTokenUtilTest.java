package com.infosupport.transapption.component.security;

import com.infosupport.transapption.model.jwt.JwtUser;
import io.jsonwebtoken.Clock;
import org.assertj.core.util.DateUtil;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JwtTokenUtilTest {

    private final String username = "username123";
    private JwtUser jwtUser;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Mock
    private Clock clock;

    @InjectMocks
    private JwtTokenUtil jwtTokenUtil;

    @Before
    public void setUp() {
        Collection<GrantedAuthority> role = Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER"));
        jwtUser = new JwtUser(1L, username, "password", role);
        // initialize @Value variables
        ReflectionTestUtils.setField(jwtTokenUtil, "secret", "QueenVictoria");
        ReflectionTestUtils.setField(jwtTokenUtil, "expiration", 60L);
        ReflectionTestUtils.setField(jwtTokenUtil, "header", "Authorization");
    }

    @Test
    public void generateDifferentTokens() {
        when(clock.now())
                .thenReturn(DateUtil.yesterday())
                .thenReturn(DateUtil.now());
        String firstToken = jwtTokenUtil.generateToken(jwtUser);
        String secondToken = jwtTokenUtil.generateToken(jwtUser);
        assertNotEquals(firstToken, secondToken);
    }

    @Test
    public void retrieveUsername() {
        when(clock.now()).thenReturn(DateUtil.now());
        String token = jwtTokenUtil.generateToken(jwtUser);
        assertEquals(username, jwtTokenUtil.getUsernameFromToken(token));
    }

    @Test
    public void retrieveExpirationFromToken() {
        Long oneMinute = 60000L;
        final Date now = DateUtil.now();
        when(clock.now()).thenReturn(now);
        String token = jwtTokenUtil.generateToken(jwtUser);
        Date expirationDate = jwtTokenUtil.getExpirationDateFromToken(token);
        assertThat(now).isCloseTo(expirationDate, oneMinute);
    }

    @Test
    public void canRefreshToken() {
        when(clock.now())
                .thenReturn(DateUtil.now())
                .thenReturn(DateUtil.tomorrow());
        String token = jwtTokenUtil.generateToken(jwtUser);
        String newToken = jwtTokenUtil.refreshToken(token);
        Date tokenDate = jwtTokenUtil.getIssuedDateFromToken(token);
        Date newTokenDate = jwtTokenUtil.getIssuedDateFromToken(newToken);
        assertThat(tokenDate).isBefore(newTokenDate);
    }

    @Test
    public void validateValidToken() {
        when(clock.now()).thenReturn(DateUtil.now());
        String token = jwtTokenUtil.generateToken(jwtUser);
        Boolean valid = jwtTokenUtil.validateToken(token, jwtUser);
        assertTrue(valid);
    }

    @Test
    public void validateInvalidToken() {
        when(clock.now()).thenReturn(DateUtil.now());
        String token = jwtTokenUtil.generateToken(jwtUser);
        JwtUser otherJwtUser = new JwtUser(1L, "sdfijf", "dsifjosf", null);
        Boolean valid = jwtTokenUtil.validateToken(token, otherJwtUser);
        assertFalse(valid);
    }

    @Test
    public void getToken() {
        String token = "OiJIUzI1NiJ9.eyJzdWIiOiIxMjM$";
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("Authorization", "Bearer "+token);
        String retrievedToken = jwtTokenUtil.getToken(request);
        assertEquals(token, retrievedToken);
    }

    @Test
    public void noBearerNoToken() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("Authorization", "Definitely not Bearer");
        String token = jwtTokenUtil.getToken(request);
        assertNull(token);
    }

    @Test
    public void noHeaderNoToken() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("Something else", "Bearer ");
        String token = jwtTokenUtil.getToken(request);
        assertNull(token);
    }

}