package com.infosupport.transapption.service.unit;

import com.infosupport.transapption.model.entity.BankAccount;
import com.infosupport.transapption.model.entity.Transaction;
import com.infosupport.transapption.model.entity.User;
import com.infosupport.transapption.model.exception.UserNotCreatedException;
import com.infosupport.transapption.service.BankAccountService;
import com.infosupport.transapption.service.RegistrationService;
import com.infosupport.transapption.service.UserService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RegistrationServiceUnitTest {

    private Integer bonus;

    private User admin;
    private BankAccount bank;

    private User user;
    private User savedUser;
    private BankAccount bankAccount;
    private Transaction transaction;

    @Mock
    private UserService userService;

    @Mock
    private BankAccountService bankAccountService;

    @Mock
    private BCryptPasswordEncoder passwordEncoder;

    @InjectMocks
    private RegistrationService registrationService;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void setUp() {
        bonus = 100;
        admin = new User(1L, "admin", "admin", "ROLE_ADMIN");
        user = new User("Username123", "Password123!", null);
        savedUser = new User(2L, "Username123", "Password123!", "ROLE_USER");
        bank = new BankAccount(1L, admin, 100L, -100L, "bank");
        bankAccount = new BankAccount(5L, savedUser, 100L, 0L, "description");
        transaction = new Transaction(1L, bank, bankAccount, 100, "01-02-2018 23:59:59", "description");
        // mocking behaviour of other classes
        when(userService.saveUser(any(User.class)))
                .thenReturn(savedUser);
        when(bankAccountService.addTransaction(any(Transaction.class)))
                .thenReturn(transaction);
        when(passwordEncoder.encode(any(String.class)))
                .thenReturn(user.getPassword());
    }

    /*
        Test creating a new User
        creating a standard bank account
        and perform a transaction
     */
    @Test
    public void addNewUser() {
        Transaction result = registrationService.register(user);
        // verify if methods are being called
        Mockito.verify(userService, Mockito.times(1)).saveUser(any(User.class));
        Mockito.verify(bankAccountService, Mockito.times(1)).saveBankAccount(any(BankAccount.class));
        Mockito.verify(bankAccountService, Mockito.times(1)).addTransaction(any(Transaction.class));
        // expect the transaction to contain the expected fields
        assertNotNull(result);
        assertEquals(savedUser, result.getReceiver().getAccountHolder());
        assertEquals(admin, result.getSender().getAccountHolder());
        assertEquals(bankAccount, result.getReceiver());
        assertEquals(bank, result.getSender());
        assertEquals(bonus, result.getAmount());
    }


    @Test
    public void addNoNewUser() {
        when(bankAccountService.addTransaction(any(Transaction.class)))
                .thenReturn(null);
        // expect an exception
        exception.expect(UserNotCreatedException.class);
        // run the actual method
        Transaction result = registrationService.register(user);
    }
}