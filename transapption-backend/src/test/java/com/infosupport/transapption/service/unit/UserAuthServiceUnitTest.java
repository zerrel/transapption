package com.infosupport.transapption.service.unit;

import com.infosupport.transapption.model.entity.User;
import com.infosupport.transapption.repository.UserRepository;
import com.infosupport.transapption.service.UserAuthService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;

/*
    Unit Test for UserAuthService
 */
@RunWith(MockitoJUnitRunner.class)
public class UserAuthServiceUnitTest {
    private final String USERNAME = "username";
    private final String PASSWORD = "password";
    private final String USER_ROLE = "ROLE_USER";

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserAuthService userAuthService;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    /*
        Method can find User and the username of the User is
        equal to the username of the UserDetails object.
     */
    @Test
    public void loadUserByUsernameHappyPath() {
        Mockito.when(userRepository.findByUsername(any(String.class)))
                .thenReturn(Optional.of(new User(USERNAME, PASSWORD, USER_ROLE)));
        UserDetails userDetails = userAuthService.loadUserByUsername(USERNAME);
        assertEquals(userDetails.getUsername(), USERNAME);
    }

    /*
        Method cant find User in database, thus throws exception
     */
    @Test
    public void loadUserByUsernameThrowsException() {
        Mockito.when(userRepository.findByUsername(any(String.class)))
                .thenReturn(Optional.empty());
        exception.expect(UsernameNotFoundException.class);
        userAuthService.loadUserByUsername(USERNAME);
    }

    /*
        Role being 'null', SimpleGrantedAuthority throws IllegalArgumentException
     */
    @Test
    public void loadUserByUsernameSadPath() {
        Mockito.when(userRepository.findByUsername(any(String.class)))
                .thenReturn(Optional.of(new User(USERNAME, PASSWORD, null)));
        exception.expect(IllegalArgumentException.class);
        userAuthService.loadUserByUsername(USERNAME);
    }
}