package com.infosupport.transapption.service.integration;

import com.infosupport.transapption.model.entity.User;
import com.infosupport.transapption.model.exception.EntityNotFoundException;
import com.infosupport.transapption.service.UserService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class UserServiceIntTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Autowired
    private UserService userService;

    @Test
    public void saveUser() {
        String encodedPassword = "$2y$12$.ryckjliJMRpBSiFGj9O2OCMDg1thCF1aw0qXrhfRo/TN2qBqf/xa";
        User user = new User("username629", encodedPassword, "ROLE_USER");
        User savedUser = userService.saveUser(user);
        // assertions
        assertNotNull(savedUser);
        assertNotNull(savedUser.getUserId());
        assertEquals(user.getUsername(), savedUser.getUsername());
        assertEquals(user.getPassword(), savedUser.getPassword());
        assertEquals(user.getRole(), savedUser.getRole());
    }

    @Test
    public void getUserById() {
        Long userId = 1L;
        User retrievedUser = userService.getUserById(userId);
        // assertions
        assertNotNull(retrievedUser);
        assertEquals(userId, retrievedUser.getUserId());
    }

    @Test
    public void getNonExistentUserById() {
        Long userId = 2383289238L;
        exception.expect(EntityNotFoundException.class);
        userService.getUserById(userId);
    }

    @Test
    public void getUserByUsername() {
        String username = "admin";
        User retrievedUser = userService.getUserByUsername(username);
        //assertions
        assertNotNull(retrievedUser);
        assertEquals(username, retrievedUser.getUsername());
    }

    @Test
    public void getNonExistentUserByUsername() {
        String username = "Ow1oSJoOkw!";
        exception.expect(EntityNotFoundException.class);
        userService.getUserByUsername(username);
    }

    @Test
    public void getAllUsers() {
        List<User> users = userService.getAllUsers();
        assertFalse(users.isEmpty());
    }

    @Test
    public void userExists() {
        String username = "admin";
        Boolean notExists = userService.userNotExists(username);
        assertFalse(notExists);
    }

    @Test
    public void userNotExists() {
        String username = "JiwjedkI372*&3";
        Boolean notExists = userService.userNotExists(username);
        assertTrue(notExists);
    }
}
