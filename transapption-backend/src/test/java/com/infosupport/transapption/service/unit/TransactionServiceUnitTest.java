package com.infosupport.transapption.service.unit;

import com.infosupport.transapption.model.entity.Transaction;
import com.infosupport.transapption.model.exception.EntityNotFoundException;
import com.infosupport.transapption.repository.TransactionRepository;
import com.infosupport.transapption.service.TransactionService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceUnitTest {
    // Transaction variables
    private final Long TRANSACTION_ID = 1L;

    @Mock
    private TransactionRepository transactionRepository;

    @InjectMocks
    private TransactionService transactionService;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void saveTransactionHappyPath() {
        Mockito.when(transactionRepository.save(any(Transaction.class)))
                .thenReturn(new Transaction());
        assertThat(transactionService.saveTransaction(new Transaction()), instanceOf(Transaction.class));
    }

    /*
        TransactionService tries to retrieve Transaction from repository,
        repository returns a Transaction object
     */
    @Test
    public void getTransactionByIdHappyPath() {
        Mockito.when(transactionRepository.findById(any(Long.class)))
                .thenReturn(Optional.of(new Transaction()));
        assertNotNull(transactionService.getTransactionById(TRANSACTION_ID));
    }

    /*
        TransactionService tries to retrieve Transaction from repository,
        repository returns empty Optional object, so TransactionService throws exception
     */
    @Test
    public void getTransactionByIdThrowException() {
        Mockito.when(transactionRepository.findById(any(Long.class)))
                .thenReturn(Optional.empty());
        exception.expect(EntityNotFoundException.class);
        transactionService.getTransactionById(TRANSACTION_ID);
    }

    @Test
    public void getAllTransactions() {
        Mockito.when(transactionRepository.findAll())
                .thenReturn(new ArrayList<>());
        assertNotNull(transactionService.getAllTransactions());
    }

    @Test
    public void getAllTransactionsByBankAccountId() {
        Mockito.when(transactionRepository.getTransactionsByBankAccountId(any(Long.class)))
                .thenReturn(new ArrayList<>());
        assertNotNull(transactionService.getTransactionsByBankAccountId(TRANSACTION_ID));
    }
}