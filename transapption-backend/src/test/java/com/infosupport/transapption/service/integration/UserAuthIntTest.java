package com.infosupport.transapption.service.integration;

import com.infosupport.transapption.service.UserAuthService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class UserAuthIntTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Autowired
    private UserAuthService userAuthService;

    @Test
    public void loadUserByUsernameThrowsException() {
        String username = "kweui&!!$$$";
        exception.expect(UsernameNotFoundException.class);
        userAuthService.loadUserByUsername(username);
    }

    @Test
    public void loadUserByUsernameHappyPath() {
        String username = "admin";
        UserDetails userDetails = userAuthService.loadUserByUsername(username);
    }
}
