package com.infosupport.transapption.service.integration;

import com.infosupport.transapption.model.entity.BankAccount;
import com.infosupport.transapption.model.entity.Transaction;
import com.infosupport.transapption.model.entity.User;
import com.infosupport.transapption.service.RegistrationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class RegistrationServiceIntTest {

    @Value("${startup.user.role}")
    private String roleUser;

    @Value("${startup.bank_account.description}")
    private String bankAccountDescription;

    @Value("${startup.transaction.bonus}")
    private Integer bonus;

    @Value("${startup.transaction.description}")
    private String transactionDescription;

    @Value("${startup.transaction.bank_account.id}")
    private Long bankAccountId;

    @Value("${startup.bank_account.balance}")
    private Long bankAccountBalance;

    @Value("${startup.bank_account.minimum_balance}")
    private Long bankAccountMinimumBalance;

    @Autowired
    private RegistrationService registrationService;

    @Test
    public void addTransaction() {
        User user = new User("Username2729", "Ijewo723&2", null);
        Transaction transaction = registrationService.register(user);
        BankAccount bankAccount = transaction.getReceiver();
        User newUser = transaction.getReceiver().getAccountHolder();
        // transaction related assertions
        assertNotNull(transaction);
        assertEquals(transactionDescription, transaction.getDescription());
        assertEquals(bankAccountId, transaction.getSender().getBankAccountId());
        // bank account related assertions
        assertNotNull(bankAccount);
        assertEquals(Long.valueOf(bonus), bankAccount.getBalance());
        assertEquals(bankAccountDescription, bankAccount.getDescription());
        assertEquals(bankAccountMinimumBalance, bankAccount.getMinimumBalance());
        // new user related assertions
        assertNotNull(user);
        assertEquals(user.getUsername(), newUser.getUsername());
        assertNotEquals(user.getPassword(), newUser.getPassword());
        assertEquals(roleUser, newUser.getRole());
    }

}
