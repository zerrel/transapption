package com.infosupport.transapption.service.integration;

import com.infosupport.transapption.model.entity.BankAccount;
import com.infosupport.transapption.model.entity.Transaction;
import com.infosupport.transapption.model.exception.EntityNotFoundException;
import com.infosupport.transapption.service.BankAccountService;
import com.infosupport.transapption.service.TransactionService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class TransactionServiceIntTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private BankAccountService bankAccountService;

    @Test
    public void saveTransaction() {
        BankAccount sender = bankAccountService.getBankAccountById(1L);
        BankAccount receiver = bankAccountService.getBankAccountById(2L);
        Transaction transaction = new Transaction(sender, receiver, 100, "31-12-1901 12:59:59", "description");
        Transaction savedTransaction = transactionService.saveTransaction(transaction);
        assertNotNull(savedTransaction);
    }

    @Test
    public void getTransactionByIdThrowsException() {
        Long transactionId = 1328392398L;
        exception.expect(EntityNotFoundException.class);
        transactionService.getTransactionById(transactionId);
    }

    @Test
    public void getTransactionByIdHappyPath() {
        Long transactionId = 1L;
        Transaction transaction = transactionService.getTransactionById(transactionId);
        assertNotNull(transaction);
    }

    @Test
    public void getAllTransactions() {
        List<Transaction> transactions = transactionService.getAllTransactions();
        assertFalse(transactions.isEmpty());
    }

    @Test
    public void getTransactionsByBankAccountId() {
        Long bankAccountId = 1L;
        List<Transaction> transactions = transactionService.getTransactionsByBankAccountId(bankAccountId);
        assertFalse(transactions.isEmpty());
    }
}
