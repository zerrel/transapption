package com.infosupport.transapption.service.unit;

import com.infosupport.transapption.model.entity.BankAccount;
import com.infosupport.transapption.model.entity.Transaction;
import com.infosupport.transapption.model.entity.User;
import com.infosupport.transapption.model.exception.EntityNotFoundException;
import com.infosupport.transapption.repository.BankAccountRepository;
import com.infosupport.transapption.service.BankAccountService;
import com.infosupport.transapption.service.TransactionService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BankAccountServiceUnitTest {

    private User user;
    private User user2;
    private BankAccount bankAccount;
    private BankAccount bankAccount2;
    private Transaction transaction;

    @Mock
    private BankAccountRepository bankAccountRepository;

    @Mock
    private TransactionService transactionService;

    @InjectMocks
    private BankAccountService bankAccountService;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void init() {
        user = new User(1L, "Username123", "Password123!", "ROLE_USER");
        user2 = new User(5L, "Username233", "Password123!", "ROLE_USER");
        bankAccount = new BankAccount(2L, user, 100L, 0L, "description");
        bankAccount2 = new BankAccount(3L, user2, 100L, 0L, "description");
        transaction = new Transaction(6L, bankAccount, bankAccount2, 10, null, "description");
        ReflectionTestUtils.setField(bankAccountService, "dateFormat", "dd-MM-YYYY HH:mm:ss");
    }

    /*
        BankAccountService tries to save BankAccount using repository,
        repository returns a BankAccount object
     */
    @Test
    public void saveBankAccountHappyPath() {
        when(bankAccountRepository.save(any(BankAccount.class)))
                .thenReturn(bankAccount);
        assertThat(bankAccountService.saveBankAccount(bankAccount), instanceOf(BankAccount.class));
    }

    /*
        BankAccountService tries to retrieve BankAccount from repository,
        repository returns a BankAccount object
     */
    @Test
    public void getBankAccountByIdHappyPath() {
        when(bankAccountRepository.findById(any(Long.class)))
                .thenReturn(Optional.of(bankAccount));
        assertThat(bankAccountService.getBankAccountById(bankAccount.getBankAccountId()), instanceOf(BankAccount.class));
    }

    /*
        BankAccountService tries to retrieve BankAccount from repository,
        repository returns empty Optional object, thus BankAccountService throws exception
     */
    @Test
    public void getBankAccountByIdThrowsException() {
        when(bankAccountRepository.findById(any(Long.class)))
                .thenReturn(Optional.empty());
        exception.expect(EntityNotFoundException.class);
        bankAccountService.getBankAccountById(bankAccount.getBankAccountId());
    }

    /*
        BankAccountService tries to retrieve a list of all BankAccounts,
        repository returns an ArrayList
     */
    @Test
    public void getAllBankAccounts() {
        when(bankAccountRepository.findAll())
                .thenReturn(new ArrayList<>());
        assertThat(bankAccountService.getAllBankAccounts(), instanceOf(List.class));
    }

    /*
        BankAccountService tries to retrieve a list of BankAccount by UserId,
        repository returns an ArrayList
     */
    @Test
    public void getBankAccountByUserIdHappyPath() {
        when(bankAccountRepository.getBankAccountsByUserId(any(Long.class)))
                .thenReturn(new ArrayList<>());
        assertThat(bankAccountService.getBankAccountsByUserId(user.getUserId()), instanceOf(List.class));
    }

    /*
        Perform a transaction
     */
    @Test
    public void addTransaction() {
        // result of performing the transaction
        Transaction transactionResult = transaction;
        // mock functionality
        when(transactionService.saveTransaction(any(Transaction.class)))
                .thenReturn(transactionResult);
        // run the actual method
        bankAccountService.addTransaction(transaction);
        // verifications
        ArgumentCaptor<BankAccount> captor = ArgumentCaptor.forClass(BankAccount.class);
        Mockito.verify(transactionService).saveTransaction(any(Transaction.class));
        Mockito.verify(bankAccountRepository, times(2)).save(captor.capture());
        // results of private method
        transactionResult.setReceiver(captor.getAllValues().get(0));
        transactionResult.setSender(captor.getAllValues().get(1));
        // results
        assertNotNull(transaction);
        assertEquals(transaction.getReceiver(), transactionResult.getReceiver());
        assertEquals(transaction.getSender(), transactionResult.getSender());
        assertEquals(transaction.getSender().getBalance(), transactionResult.getSender().getBalance());
        assertEquals(transaction.getReceiver().getBalance(), transactionResult.getReceiver().getBalance());
    }

}