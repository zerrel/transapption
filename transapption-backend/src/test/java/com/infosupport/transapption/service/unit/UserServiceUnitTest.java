package com.infosupport.transapption.service.unit;

import com.infosupport.transapption.model.entity.User;
import com.infosupport.transapption.model.exception.EntityNotFoundException;
import com.infosupport.transapption.repository.UserRepository;
import com.infosupport.transapption.service.UserService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;

/*
    Unit Test for UserService class
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceUnitTest {

    private User user;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void init() {
        user = new User(1L, "Username123", "Password123!", "ROLE_USER");
    }

    @Test
    public void createNewUserHappyPath(){
        Mockito.when(userRepository.save(any(User.class))).thenReturn(user);
        assertNotNull(userService.saveUser(user));
    }

    @Test
    public void createNewAccountReturnsNull(){
        Mockito.when(userRepository.save(any(User.class))).thenReturn(null);
        assertNull(userService.saveUser(user));
    }

    /*
        UserService tries to find nonexistent User using the id
     */
    @Test
    public void getUserByIdThrowsException(){
        Mockito.when(userRepository.findById(any(Long.class))).thenReturn(Optional.empty());
        exception.expect(EntityNotFoundException.class);
        userService.getUserById(user.getUserId());
    }

    /*
        UserService finds existing User using the id
     */
    @Test
    public void getUserByIdHappyPath(){
        Mockito.when(userRepository.findById(any(Long.class))).thenReturn(Optional.of(new User()));
        assertNotNull(userService.getUserById(user.getUserId()));
    }

    /*
        UserService tries to find nonexistent User using the username
     */
    @Test
    public void getUserByUsernameThrowsException(){
        Mockito.when(userRepository.findByUsername(any(String.class))).thenReturn(Optional.empty());
        exception.expect(EntityNotFoundException.class);
        userService.getUserByUsername(user.getUsername());
    }

    /*
        UserService finds existing User using the username
     */
    @Test
    public void getUserByUsernameHappyPath() {
        Mockito.when(userRepository.findByUsername(any(String.class))).thenReturn(Optional.of(new User()));
        assertNotNull(userService.getUserByUsername(user.getUsername()));
    }

    /*
        UserService finds existing user by username
     */
    @Test
    public void userExists(){
        Mockito.when(userRepository.findByUsername(any(String.class))).thenReturn(Optional.of(new User()));
        assertFalse(userService.userNotExists(user.getUsername()));
    }

    /*
        UserService cant find user by username, thus user doesn't exist
     */
    @Test
    public void userDoesNotExist(){
        Mockito.when(userRepository.findByUsername(any(String.class))).thenReturn(Optional.empty());
        assertTrue(userService.userNotExists(user.getUsername()));
    }

    /*
        UserService retrieves all Users
     */
    @Test
    public void getAllUser() {
        Mockito.when(userRepository.findAll()).thenReturn(new ArrayList<>());
        assertThat(userService.getAllUsers(), instanceOf(List.class));
    }
}