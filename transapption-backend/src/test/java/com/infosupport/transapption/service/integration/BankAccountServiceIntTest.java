package com.infosupport.transapption.service.integration;

import com.infosupport.transapption.model.entity.BankAccount;
import com.infosupport.transapption.model.entity.Transaction;
import com.infosupport.transapption.model.entity.User;
import com.infosupport.transapption.model.exception.EntityNotFoundException;
import com.infosupport.transapption.service.BankAccountService;
import com.infosupport.transapption.service.UserService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class BankAccountServiceIntTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Autowired
    private BankAccountService bankAccountService;

    @Autowired
    private UserService userService;

    /*
        Does not work with existing user entity
     */
    @Test
    public void saveBankAccount() {
        User accountHolder = userService.getUserById(1L);
        BankAccount bankAccount = new BankAccount(accountHolder, 100L, 0L, "some description");
        BankAccount savedBankAccount = bankAccountService.saveBankAccount(bankAccount);
        // assertions
        assertNotNull(savedBankAccount);
        assertNotNull(savedBankAccount.getAccountHolder());
        assertNotNull(savedBankAccount.getBankAccountId());
        assertEquals(bankAccount.getDescription(), savedBankAccount.getDescription());
    }

    @Test
    public void getBankAccountByIdThrowsException() {
        Long bankAccountId = 2039193982849L;
        exception.expect(EntityNotFoundException.class);
        bankAccountService.getBankAccountById(bankAccountId);
    }

    @Test
    public void getBankAccountById() {
        Long bankAccountId = 1L;
        BankAccount bankAccount = bankAccountService.getBankAccountById(bankAccountId);
        assertNotNull(bankAccount);
        assertEquals(bankAccountId, bankAccount.getBankAccountId());
    }

    @Test
    public void getAllBankAccount() {
        List<BankAccount> bankAccounts = bankAccountService.getAllBankAccounts();
        assertFalse(bankAccounts.isEmpty());
    }

    @Test
    public void getNoBankAccountsByUserId() {
        List<BankAccount> bankAccounts = bankAccountService.getBankAccountsByUserId(231324L);
        assertTrue(bankAccounts.isEmpty());
    }

    @Test
    public void getAllBankAccountsByUserId() {
        List<BankAccount> bankAccounts = bankAccountService.getBankAccountsByUserId(1L);
        assertFalse(bankAccounts.isEmpty());
    }

    @Test
    public void addTransaction() {
        BankAccount sender = bankAccountService.getBankAccountById(1L);
        BankAccount receiver = bankAccountService.getBankAccountById(2L);
        // save the balance
        Long senderBalance = sender.getBalance();
        Long receiverBalance = receiver.getBalance();
        // performing the actual transaction
        Transaction transaction = new Transaction(sender, receiver, 100, "description");
        Transaction savedTransaction = bankAccountService.addTransaction(transaction);
        // retrieve results
        BankAccount senderAfter = bankAccountService.getBankAccountById(1L);
        BankAccount receiverAfter = bankAccountService.getBankAccountById(2L);
        Long senderBalanceAfter = senderBalance - transaction.getAmount();
        Long receiverBalanceAfter = receiverBalance + transaction.getAmount();
        // assertions
        assertNotNull(savedTransaction);
        assertNotNull(savedTransaction.getTimestamp());
        assertEquals(senderBalanceAfter, senderAfter.getBalance());
        assertEquals(receiverBalanceAfter, receiverAfter.getBalance());
    }
}
