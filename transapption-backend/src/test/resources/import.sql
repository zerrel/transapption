INSERT INTO user (username, password, role) VALUES ('admin', '$2y$12$y4E7rCYacszXER2q/9s2s.msjSSo0HgL3/9LYkcb6DOk8ggEpUm12', 'ROLE_ADMIN');
INSERT INTO user (username, password, role) VALUES ('username222', '$2y$12$17HRmnKGtYSE9.Zr.RNw6eQf/gHZZ0JktPthjtLCWOkcT7ULLBbAq', 'ROLE_USER');
INSERT INTO user (username, password, role) VALUES ('username333', '$2y$12$Hz/Ari3K/qE03/g0Gv1k.ejXdPkpMlsV7MDDSAdkXUDOwyrAxn7se', 'ROLE_USER');
INSERT INTO user (username, password, role) VALUES ('username444', '$2y$12$SefxUBRxRhBTyr5XOJZlAegZlkE2ihRc3z4wS69zom1hp98UVDqjG', 'ROLE_USER');
INSERT INTO user (username, password, role) VALUES ('username555', '$2y$12$QvTbxmfxrqiVIyPK0U0XT.kc6ut5JjD5fZ9yH6.SpyLTSRecZlHtK', 'ROLE_USER');

INSERT INTO bank_account (account_holder_id, balance, description, minimum_balance) VALUES (1, 9999999999999, 'bank', -999999999999);
INSERT INTO bank_account (account_holder_id, balance, description, minimum_balance) VALUES (2, 999, 'new bank account', 0);

INSERT INTO transaction (sender_id, receiver_id, amount, description) VALUES (1, 2, 100, 'some transaction');