package com.infosupport.transapption.component.validation;

import com.infosupport.transapption.component.UserPrincipalExtractor;
import com.infosupport.transapption.model.dto.BankAccountDto;
import com.infosupport.transapption.model.entity.User;
import com.infosupport.transapption.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class BankAccountDtoValidator implements Validator {

    private String balanceField = "balance";
    private String minimumBalanceField = "minimumBalance";
    private String accountHolderIdField = "accountHolderId";
    private String adminRole = "ROLE_ADMIN";

    private final UserService userService;
    private final UserPrincipalExtractor userPrincipalExtractor;

    @Autowired
    public BankAccountDtoValidator(UserService userService, UserPrincipalExtractor userPrincipalExtractor) {
        this.userService = userService;
        this.userPrincipalExtractor = userPrincipalExtractor;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return BankAccountDto.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        BankAccountDto bankAccount = (BankAccountDto) target;
        User currentUser = userPrincipalExtractor.getCurrentUser();
        validateAccountHolder(bankAccount.getAccountHolderId(), currentUser, errors);
        validateMinimumBalance(bankAccount.getMinimumBalance(), currentUser, errors);
        validateBalance(bankAccount.getBalance(), errors);
    }

    /*
        Validate whether there exists a User with the given id
        and if the creator of the BankAccount is the logged in User
        or if the current User has the role 'ROLE_ADMIN'
     */
    private void validateAccountHolder(Long accountHolderId, User currentUser, Errors errors) {
        if(accountHolderId != null) {
            if (userService.getUserById(accountHolderId) == null) {
                errors.rejectValue(accountHolderIdField, "bankAccount.accountHolder.exists",
                        "There doesn't exist any User with this id.");
            } else if (!accountHolderId.equals(currentUser.getUserId()) && !currentUser.getRole().equals(adminRole)) {
                errors.rejectValue(accountHolderIdField, "bankAccount.accountHolder.owner",
                        "The logged in User is not the owner of the BankAccount.");
            }
        }
    }

    /*
        Validate whether the minimumBalance has been set
        and if the current User has the role 'ROLE_ADMIN'
     */
    private void validateMinimumBalance(Long minimumBalance, User currentUser, Errors errors) {
        if (minimumBalance != null && minimumBalance != 0 && !currentUser.getRole().equals(adminRole)) {
            errors.rejectValue(minimumBalanceField, "bankAccount.minimumBalance.zero",
                    "The minimumBalance has to be zero.");
        }
    }

    /*
        Validate whether the balance has been set
     */
    private void validateBalance(Long balance, Errors errors) {
        if (balance != null && balance != 0) {
            errors.rejectValue(balanceField, "balance.balance.zero",
                    "The balance has to be zero");
        }
    }

}
