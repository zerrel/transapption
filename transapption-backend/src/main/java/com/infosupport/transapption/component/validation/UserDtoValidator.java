package com.infosupport.transapption.component.validation;

import com.infosupport.transapption.model.dto.UserDto;
import com.infosupport.transapption.service.UserService;
import org.passay.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Arrays;

@Component
public class UserDtoValidator implements Validator {

    private final UserService userService;

    @Autowired
    public UserDtoValidator(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return UserDto.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserDto user = (UserDto) target;
        validateUsername(user.getUsername(), errors);
        validatePassword(user.getPassword(), errors);
        validateConfirmPassword(user.getConfirmPassword(), user.getPassword(), errors);
    }

    /*
        Validate whether the username is available
     */
    private void validateUsername(String username, Errors errors) {
        // username already in use?
        if(username != null && !userService.userNotExists(username)) {
            errors.rejectValue("username", "user.username.taken",
                    "This username is already in use.");
        }
    }

    /*
        Validate whether the password is strong enough
     */
    private void validatePassword(String password, Errors errors) {
        // rules for validating the password
        PasswordValidator validator = new PasswordValidator(Arrays.asList(
            // between 8 and 30 characters
            new LengthRule(8, 30),
            // contain at least 1 uppercase character
            new CharacterRule(EnglishCharacterData.UpperCase, 1),
            // contain at least 1 lowercase character
            new CharacterRule(EnglishCharacterData.LowerCase, 1),
            // contain at least 1 numerical character
            new CharacterRule(EnglishCharacterData.Digit, 1),
            // contain at least 1 special character
            new CharacterRule(EnglishCharacterData.Special,1),
            // contain at least 3 alphabetical characters
            new CharacterRule(EnglishCharacterData.Alphabetical, 6),
            // no whitespace
            new WhitespaceRule()));
        // validate the password according to the above rules
        if(password != null) {
            RuleResult result = validator.validate(new PasswordData(password));
            if(!result.isValid()) {
                errors.rejectValue("password", "user.password.weak",
                        "The password you have chosen is too weak.");
            }
        }
    }

    /*
        Validate whether the password matches the confirm password
     */
    private void validateConfirmPassword(String confirmPassword, String password, Errors errors) {
        // confirmPassword field does not match the password field
        if(confirmPassword != null && !confirmPassword.equals(password)) {
            errors.rejectValue("password", "user.password.match",
                    "The passwords do not match.");
        }
    }

}