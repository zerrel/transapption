package com.infosupport.transapption.component.validation;

import com.infosupport.transapption.component.UserPrincipalExtractor;
import com.infosupport.transapption.model.dto.TransactionDto;
import com.infosupport.transapption.model.entity.BankAccount;
import com.infosupport.transapption.service.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.math.BigInteger;

@Component
public class TransactionDtoValidator implements Validator {

    private String amountField = "amount";
    private String senderField = "senderId";
    private String receiverField = "receiverId";

    private final BankAccountService bankAccountService;
    private final UserPrincipalExtractor userPrincipalExtractor;

    @Autowired
    public TransactionDtoValidator(BankAccountService bankAccountService, UserPrincipalExtractor userPrincipalExtractor) {
        this.bankAccountService = bankAccountService;
        this.userPrincipalExtractor = userPrincipalExtractor;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return TransactionDto.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        TransactionDto transaction = (TransactionDto) target;
        validateSender(transaction.getSenderId(), transaction.getAmount(), errors);
        validateReceiver(transaction.getReceiverId(), transaction.getAmount(), errors);
        validateSenderAndReceiver(transaction.getSenderId(), transaction.getReceiverId(), errors);
    }

    /*
        Validate whether the sender Bank Account exists
        and if the sender is the logged in user
        or if the current User has the role 'ROLE_ADMIN'
        and if the BankAccount of the sender has sufficient credits
     */
    private void validateSender(Long sender, Integer amount, Errors errors) {
        if(sender != null) {
            BankAccount senderBankAccount = bankAccountService.getBankAccountById(sender);
            if (senderBankAccount == null) {
                errors.rejectValue(senderField, "transaction.sender.exists",
                        "The Bank Account of the sender does not exist.");
            } else if (!senderBankAccount.getAccountHolder().equals(userPrincipalExtractor.getCurrentUser()) &&
                    !userPrincipalExtractor.getCurrentUser().getRole().equals("ROLE_ADMIN")) {
                errors.rejectValue(senderField, "transaction.sender.owner",
                        "The logged in User is not the owner of this BankAccount.");
            } else if ((senderBankAccount.getBalance() - amount) < senderBankAccount.getMinimumBalance()) {
                errors.rejectValue(amountField, "transaction.amount.insufficient",
                        "The sender has insufficient credits to perform this Transaction.");
            }
        }
    }

    /*
        Validate whether the receiver BankAccount exists
        and whether the balance of the receiver exceeds the maximum Long value
     */
    private void validateReceiver(Long receiver, Integer amount, Errors errors) {
        if(receiver != null) {
            BankAccount receiverBankAccount = bankAccountService.getBankAccountById(receiver);
            if (receiverBankAccount == null) {
                errors.rejectValue(receiverField, "transaction.receiver.exists",
                        "The Bank Account of the receiver does not exist.");
            } else if (BigInteger.valueOf(receiverBankAccount.getBalance()).add(BigInteger.valueOf(amount))
                    .compareTo(BigInteger.valueOf(Long.MAX_VALUE)) > 0) {
                errors.rejectValue(amountField, "transaction.amount.max",
                        "The Bank Account of the receiver has a too high balance.");
            }
        }
    }

    /*
        Validate whether the same BankAccount is used as sender and receiver
     */
    private void validateSenderAndReceiver(Long sender, Long receiver, Errors errors) {
        if (sender != null && sender.equals(receiver)) {
            errors.rejectValue(receiverField, "transaction.receiver",
                    "The sender and receiver cannot be the same.");
        }
    }

}
