package com.infosupport.transapption.component.security;

import com.infosupport.transapption.model.jwt.JwtUser;
import com.infosupport.transapption.service.UserAuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtAuthorizationTokenFilter extends OncePerRequestFilter {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private JwtTokenUtil jwtTokenUtil;
    private final UserAuthService authService;

    @Autowired
    public JwtAuthorizationTokenFilter(JwtTokenUtil jwtTokenUtil, UserAuthService authService) {
        this.jwtTokenUtil = jwtTokenUtil;
        this.authService = authService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException, UsernameNotFoundException {
        String username;
        final String token = jwtTokenUtil.getToken(request);
        logger.info("Attempt to authenticate jwt token.");
        if(token != null) {
            username = jwtTokenUtil.getUsernameFromToken(token);
            if(username != null) {
                JwtUser jwtUser = authService.loadUserByUsername(username);
                if (jwtTokenUtil.validateToken(token, jwtUser)) {
                    UsernamePasswordAuthenticationToken authentication =
                            new UsernamePasswordAuthenticationToken(jwtUser, null, jwtUser.getAuthorities());
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }
        }
        chain.doFilter(request, response);
    }

}

