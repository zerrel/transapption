package com.infosupport.transapption.component;

import com.infosupport.transapption.model.entity.User;
import com.infosupport.transapption.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/*
    Transfers the Spring Security User to User Entity
 */
@Component
public class UserPrincipalExtractor {

    private final UserRepository userRepository;

    @Autowired
    public UserPrincipalExtractor(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /*
        Retrieve the logged in User
     */
    public User getCurrentUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if(authentication instanceof AnonymousAuthenticationToken){
            throw new AuthenticationCredentialsNotFoundException("User is not logged in");
        }else{
            return userRepository.
                findByUsername(authentication.getName())
                .orElseThrow(() -> new UsernameNotFoundException("username " + authentication.getName() + " not found"));
        }
    }
}
