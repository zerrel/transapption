package com.infosupport.transapption.component.security;

import com.infosupport.transapption.model.jwt.JwtUser;
import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.DefaultClock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtTokenUtil implements Serializable {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration}")
    private Long expiration;

    @Value("${jwt.header}")
    private String header;

    private Clock clock = DefaultClock.INSTANCE;
    private SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS512;

    String getUsernameFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    Date getIssuedDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getIssuedAt);
    }

    Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    private <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        if(claims != null) {
            return claimsResolver.apply(claims);
        }
        return null;
    }

    private Claims getAllClaimsFromToken(String token) {
        Claims claims = null;
        try {
            claims = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (UnsupportedJwtException | MalformedJwtException | IllegalArgumentException | SignatureException e) {
            logger.warn("Invalid Jwt token");
        } catch (ExpiredJwtException e) {
            logger.warn("Expired Jwt token");
        }
        return claims;
    }

    public String generateToken(JwtUser jwtUser) {
        Map<String, Object> claims = new HashMap<>();
        final Date createdDate = clock.now();
        final Date expirationDate = calculateExpirationDate(createdDate);
        // give additional variables to the claims
        claims.put("userId", jwtUser.getId());
        claims.put("role", jwtUser.getRole());
        // create the jwt token
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(jwtUser.getUsername())
                .setIssuedAt(createdDate)
                .setExpiration(expirationDate)
                .signWith(signatureAlgorithm, secret)
                .compact();
    }

    public String refreshToken(String token) {
        final Date createdDate = clock.now();
        final Date expirationDate = calculateExpirationDate(createdDate);
        // create claims
        Claims claims = getAllClaimsFromToken(token);
        if(claims != null) {
            claims.setIssuedAt(createdDate);
            claims.setExpiration(expirationDate);
            // build the jwt token
            return Jwts.builder()
                    .setClaims(claims)
                    .signWith(signatureAlgorithm, secret)
                    .compact();
        }
        return null;
    }

    public String getToken(HttpServletRequest request) {
        String authHeader = request.getHeader(header);
        if(authHeader != null && authHeader.startsWith("Bearer ")) {
            return authHeader.substring(7);
        }
        return null;
    }

    public Boolean validateToken(String token, JwtUser jwtUser) {
        String username = getUsernameFromToken(token);
        if(username != null && username.equals(jwtUser.getUsername()) && !isExpired(token)) {
            logger.info("Token is still valid.");
            return true;
        }
        logger.info("Token is invalid or expired.");
        return false;
    }

    private Boolean isExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(clock.now());
    }

    private Date calculateExpirationDate(Date createdDate) {
        return new Date(createdDate.getTime() + expiration * 1000);
    }
}
