package com.infosupport.transapption.service;

import com.infosupport.transapption.model.entity.Transaction;
import com.infosupport.transapption.model.exception.EntityNotFoundException;
import com.infosupport.transapption.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TransactionService {

    private final TransactionRepository transactionRepository;

    @Autowired
    public TransactionService(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public Transaction saveTransaction(Transaction transaction){
        return transactionRepository.save(transaction);
    }

    public Transaction getTransactionById(Long id){
        return transactionRepository.findById(id)
            .orElseThrow(() -> new EntityNotFoundException("Transaction with id "+id+" could not be found"));
    }

    public List<Transaction> getTransactionsByBankAccountId(Long id){
        return transactionRepository.getTransactionsByBankAccountId(id);
    }

    public List<Transaction> getAllTransactions (){
        return new ArrayList<>(transactionRepository.findAll());
    }

}
