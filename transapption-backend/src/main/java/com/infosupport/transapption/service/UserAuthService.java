package com.infosupport.transapption.service;

import com.infosupport.transapption.model.entity.User;
import com.infosupport.transapption.model.jwt.JwtUser;
import com.infosupport.transapption.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class UserAuthService implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public UserAuthService(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    /*
        Retrieves the user during login
     */
    @Override
    public JwtUser loadUserByUsername(String username) {
        User user = userRepository.findByUsername(username)
                                .orElseThrow(() -> new UsernameNotFoundException("No user found with username "+username));
        return new JwtUser(user.getUserId(),
                user.getUsername(),
                user.getPassword(),
                getGrantedAuthorities(user.getRole()));
    }

    /*
        Transfer role to a collection of granted authorities
     */
    private Collection<? extends GrantedAuthority> getGrantedAuthorities(String role) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(role));
        return authorities;
    }

}