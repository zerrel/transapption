package com.infosupport.transapption.service;

import com.infosupport.transapption.model.entity.BankAccount;
import com.infosupport.transapption.model.entity.Transaction;
import com.infosupport.transapption.model.exception.EntityNotFoundException;
import com.infosupport.transapption.repository.BankAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
    CRUD functionality for Bank Accounts,
    and functionality for performing a transaction.
 */
@Service
public class BankAccountService {

    @Value("${transfer.date_format}")
    private String dateFormat;

    private final BankAccountRepository bankAccountRepository;
    private final TransactionService transactionService;

    @Autowired
    public BankAccountService(BankAccountRepository bankAccountRepository, TransactionService transactionService) {
        this.bankAccountRepository = bankAccountRepository;
        this.transactionService = transactionService;
    }

    public BankAccount saveBankAccount(BankAccount bankAccount){
        return bankAccountRepository.save(bankAccount);
    }

    /*
        Perform the actual transaction
        Do a rollback in case something fails
     */
    @Transactional
    public Transaction addTransaction(Transaction transaction) {
        credit(transaction.getReceiver(), transaction.getAmount());
        debit(transaction.getSender(), transaction.getAmount());
        transaction.setTimestamp(new SimpleDateFormat(dateFormat).format(new Date()));
        return transactionService.saveTransaction(transaction);
    }

    private void credit(BankAccount receiver, Integer amount){
        receiver.setBalance(receiver.getBalance() + amount);
        bankAccountRepository.save(receiver);
    }

    private void debit(BankAccount sender, Integer amount){
        sender.setBalance(sender.getBalance() - amount);
        bankAccountRepository.save(sender);
    }

    public BankAccount getBankAccountById(Long id) {
        return bankAccountRepository.findById(id)
            .orElseThrow(() -> new EntityNotFoundException("BankAccount with id "+id+" could not be found"));
    }

    public List<BankAccount> getAllBankAccounts(){
        return new ArrayList<>(bankAccountRepository.findAll());
    }

    public List<BankAccount> getBankAccountsByUserId(Long userId){
        return bankAccountRepository.getBankAccountsByUserId(userId);
    }

}
