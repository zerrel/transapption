package com.infosupport.transapption.service;

import com.infosupport.transapption.model.entity.BankAccount;
import com.infosupport.transapption.model.entity.Transaction;
import com.infosupport.transapption.model.entity.User;
import com.infosupport.transapption.model.exception.UserNotCreatedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/*
    This service is used for the registration of a new User
    Whenever a User is registered, a new BankAccount will be created.
    Afterwards a Transaction will be performed that adds 100 credits
    from the Admin BankAccount to the new BankAccount.
 */
@Service
public class RegistrationService {

    @Value("${startup.user.role}")
    private String roleUser;

    @Value("${startup.bank_account.description}")
    private String bankAccountDescription;

    @Value("${startup.transaction.bonus}")
    private Integer bonus;

    @Value("${startup.transaction.description}")
    private String transactionDescription;

    @Value("${startup.transaction.bank_account.id}")
    private Long bankAccountId;

    @Value("${startup.bank_account.balance}")
    private Long bankAccountBalance;

    @Value("${startup.bank_account.minimum_balance}")
    private Long bankAccountMinimumBalance;

    private final UserService userService;
    private final BankAccountService bankAccountService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public RegistrationService(UserService userService, BankAccountService bankAccountService,
                               BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userService = userService;
        this.bankAccountService = bankAccountService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Transactional
    public Transaction register(User user) {
        User newUser = registerUser(user);
        BankAccount bankAccount = createBankAccount(newUser);
        Transaction transaction = bonusTransaction(bankAccount);
        if(transaction == null) {
            throw new UserNotCreatedException("The system was unable to register a new user.");
        }
        return transaction;
    }

    /*
        Save the User in the database
     */
    private User registerUser(User user) {
        return userService.saveUser(new User(
            user.getUsername(),
            bCryptPasswordEncoder.encode(user.getPassword()),
            roleUser));
    }

    /*
        Save a BankAccount with User as AccountHolder in the database
     */
    private BankAccount createBankAccount(User accountHolder){
        return bankAccountService.saveBankAccount(new BankAccount(
           accountHolder,
           bankAccountBalance,
           bankAccountMinimumBalance,
           bankAccountDescription));
    }

    /*
        Perform a Transaction to add a bonus to the BankAccount
     */
    private Transaction bonusTransaction(BankAccount receiver){
        BankAccount sender = bankAccountService.getBankAccountById(bankAccountId);
        return bankAccountService.addTransaction(new Transaction(
            sender,
            receiver,
            bonus,
            transactionDescription));
    }
}
