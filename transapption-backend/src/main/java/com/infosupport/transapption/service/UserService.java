package com.infosupport.transapption.service;

import com.infosupport.transapption.model.entity.User;
import com.infosupport.transapption.model.exception.EntityNotFoundException;
import com.infosupport.transapption.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User saveUser(User user) {
        return userRepository.save(user);
    }

    public User getUserById(Long id) {
        return userRepository.findById(id)
            .orElseThrow(() -> new EntityNotFoundException("User with id "+id+" could not be found"));
    }

    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username)
            .orElseThrow(() -> new EntityNotFoundException("User with username "+username+" could not be found"));
    }

    public List<User> getAllUsers() {
        return new ArrayList<>(userRepository.findAll());
    }

    public boolean userNotExists(String username) {
        return userRepository.findByUsername(username)
            .equals(Optional.empty());
    }
}
