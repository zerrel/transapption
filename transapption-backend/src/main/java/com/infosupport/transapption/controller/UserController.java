package com.infosupport.transapption.controller;

import com.infosupport.transapption.mapper.UserMapper;
import com.infosupport.transapption.model.dto.UserDto;
import com.infosupport.transapption.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@Api(tags = "User Controller", description = "Endpoints for performing GET-requests of user data.")
public class UserController {

    private final UserMapper userMapper;
    private final UserService userService;

    @Autowired
    public UserController(UserMapper userMapper, UserService userService) {
        this.userMapper = userMapper;
        this.userService = userService;
    }

    @GetMapping("/{id}")
    @ApiOperation("Endpoint for retrieving User entity by id.")
    public UserDto getUserById(@PathVariable(value = "id") Long id) {
        return userMapper.toDto(userService.getUserById(id));
    }

    @GetMapping("/all")
    @ApiOperation("Endpoint for retrieving all User entities.")
    public List<UserDto> getAllUsers() {
        return userMapper.toDto(userService.getAllUsers());
    }

    @GetMapping
    @ApiOperation("Endpoint for retrieving User entity by username.")
    public UserDto getUserByUsername(@RequestParam(value = "username") String username) {
        return userMapper.toDto(userService.getUserByUsername(username));
    }

}
