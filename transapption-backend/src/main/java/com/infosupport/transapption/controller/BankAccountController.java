package com.infosupport.transapption.controller;

import com.infosupport.transapption.component.UserPrincipalExtractor;
import com.infosupport.transapption.component.validation.BankAccountDtoValidator;
import com.infosupport.transapption.mapper.BankAccountMapper;
import com.infosupport.transapption.model.dto.BankAccountDto;
import com.infosupport.transapption.model.entity.BankAccount;
import com.infosupport.transapption.service.BankAccountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/bank-accounts")
@Api(tags = "Bank Account Controller", description = "Endpoints for accessing and creating bank accounts.")
public class BankAccountController {

    private final BankAccountMapper bankAccountMapper;
    private final BankAccountService bankAccountService;
    private final UserPrincipalExtractor userPrincipalExtractor;
    private final BankAccountDtoValidator bankAccountDtoValidator;

    @Autowired
    public BankAccountController(BankAccountMapper bankAccountMapper, BankAccountService bankAccountService,
                                 UserPrincipalExtractor userPrincipalExtractor, BankAccountDtoValidator bankAccountDtoValidator) {
        this.bankAccountMapper = bankAccountMapper;
        this.bankAccountService = bankAccountService;
        this.userPrincipalExtractor = userPrincipalExtractor;
        this.bankAccountDtoValidator = bankAccountDtoValidator;
    }

    @InitBinder
    public void setupBinder(WebDataBinder binder) {
        binder.addValidators(bankAccountDtoValidator);
    }

    @GetMapping
    @ApiOperation("Endpoint for retrieving all bank accounts owner by a user.")
    public List<BankAccountDto> bankAccountOverview() {
        return bankAccountMapper.toPrivateDto(
                bankAccountService.getBankAccountsByUserId(
                userPrincipalExtractor.getCurrentUser().getUserId()));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation("Endpoint for creating a new bank account.")
    public void addBankAccount(@Valid BankAccountDto bankAccountDto){
        BankAccount bankAccount = bankAccountMapper.toEntity(bankAccountDto);
        bankAccountService.saveBankAccount(bankAccount);
    }

    @GetMapping("/{id}")
    @ApiOperation("Endpoint for retrieving a bank account by id.")
    public BankAccountDto getBankAccountByID(@PathVariable(value = "id") Long id) {
        return bankAccountMapper.toDto(bankAccountService.getBankAccountById(id));
    }

    @GetMapping("/all")
    @ApiOperation("Endpoint for retrieving all bank accounts.")
    public List<BankAccountDto> getAllBankAccounts() {
        return bankAccountMapper.toDto(bankAccountService.getAllBankAccounts());
    }
}
