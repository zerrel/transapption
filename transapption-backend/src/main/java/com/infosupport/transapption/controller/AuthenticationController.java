package com.infosupport.transapption.controller;

import com.infosupport.transapption.component.security.JwtTokenUtil;
import com.infosupport.transapption.model.error.ApiError;
import com.infosupport.transapption.model.exception.AuthenticationException;
import com.infosupport.transapption.model.jwt.JwtAuthenticationRequest;
import com.infosupport.transapption.model.jwt.JwtAuthenticationResponse;
import com.infosupport.transapption.model.jwt.JwtUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/*
    Authentication for Jwt Token
 */
@RestController
@Api(tags = "Authentication Controller", description = "Endpoints for creating and refreshing jwt tokens.")
public class AuthenticationController {

    private final JwtTokenUtil jwtTokenUtil;
    private final AuthenticationManager authenticationManager;

    @Autowired
    public AuthenticationController(JwtTokenUtil jwtTokenUtil, AuthenticationManager authenticationManager) {
        this.jwtTokenUtil = jwtTokenUtil;
        this.authenticationManager = authenticationManager;
    }

    /*
        Try to login
        If successful, return jwt token
     */
    @PostMapping("/login")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation("Endpoint for retrieving a jwt token.")
    public JwtAuthenticationResponse createAuthenticationToken(@Valid JwtAuthenticationRequest request) {
        Authentication authentication;
        try {
             authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                             request.getUsername(), request.getPassword()));
        } catch (BadCredentialsException e) {
            // in case of bad credentials
            throw new AuthenticationException(e.getMessage(), e);
        }
        // in case of correct credentials
        final JwtUser jwtUser = (JwtUser) authentication.getPrincipal();
        final String token = jwtTokenUtil.generateToken(jwtUser);
        return new JwtAuthenticationResponse(token);
    }

    /*
        Try to refresh jwt token
        If token is still valid, refresh and return jwt token
     */
    @GetMapping("/refresh")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation("Endpoint for refreshing a jwt token.")
    public ResponseEntity<JwtAuthenticationResponse> refreshAuthenticationToken(HttpServletRequest request) {
        String token = jwtTokenUtil.getToken(request);
        if(token != null) {
            String newToken = jwtTokenUtil.refreshToken(token);
            if(newToken != null) {
                return ResponseEntity.ok(new JwtAuthenticationResponse(newToken));
            }
        }
        return ResponseEntity.badRequest().build();
    }

    @ExceptionHandler(AuthenticationException.class)
    public ResponseEntity<ApiError> handleAuthenticationException(AuthenticationException e) {
        ApiError error = new ApiError(HttpStatus.UNAUTHORIZED,
                "The user tried to login using bad credentials.", e);
        return new ResponseEntity<>(error, error.getStatus());
    }
}
