package com.infosupport.transapption.controller;

import com.infosupport.transapption.component.validation.UserDtoValidator;
import com.infosupport.transapption.mapper.UserMapper;
import com.infosupport.transapption.model.dto.UserDto;
import com.infosupport.transapption.model.entity.User;
import com.infosupport.transapption.model.error.ApiError;
import com.infosupport.transapption.model.exception.UserNotCreatedException;
import com.infosupport.transapption.service.RegistrationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Api(tags = "Registration Controller", description = "Endpoints for the registration of a new user.")
public class RegistrationController {

    private final UserMapper userMapper;
    private final RegistrationService registrationService;
    private final UserDtoValidator userDtoValidator;

    @Autowired
    public RegistrationController(UserMapper userMapper, RegistrationService registrationService,
                                  UserDtoValidator userDtoValidator) {
        this.userMapper = userMapper;
        this.registrationService = registrationService;
        this.userDtoValidator = userDtoValidator;
    }

    @InitBinder
    public void setupBinder(WebDataBinder binder) {
        binder.addValidators(userDtoValidator);
    }

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation("Endpoint for registering a new user.")
    public void register(@Valid UserDto userDto) {
        User user = userMapper.toEntity(userDto);
        registrationService.register(user);
    }

    @ExceptionHandler(UserNotCreatedException.class)
    public ResponseEntity<ApiError> userNotCreatedException(UserNotCreatedException e) {
        ApiError error = new ApiError(HttpStatus.CONFLICT, "The system was unable to create a new user.", e);
        return new ResponseEntity<>(error, error.getStatus());
    }

}
