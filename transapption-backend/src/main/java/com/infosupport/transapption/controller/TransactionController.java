package com.infosupport.transapption.controller;

import com.infosupport.transapption.component.UserPrincipalExtractor;
import com.infosupport.transapption.component.validation.TransactionDtoValidator;
import com.infosupport.transapption.mapper.BankAccountMapper;
import com.infosupport.transapption.mapper.TransactionMapper;
import com.infosupport.transapption.model.dto.BankAccountDto;
import com.infosupport.transapption.model.dto.TransactionDto;
import com.infosupport.transapption.model.entity.Transaction;
import com.infosupport.transapption.service.BankAccountService;
import com.infosupport.transapption.service.TransactionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Api(tags = "Transaction Controller", description = "Endpoints for accessing and performing transaction.")
public class TransactionController {

    private final TransactionMapper transactionMapper;
    private final BankAccountMapper bankAccountMapper;
    private final TransactionService transactionService;
    private final BankAccountService bankAccountService;
    private final UserPrincipalExtractor userPrincipalExtractor;
    private final TransactionDtoValidator transactionDtoValidator;

    @Autowired
    public TransactionController(TransactionMapper transactionMapper, BankAccountMapper bankAccountMapper, TransactionService transactionService,
                                 BankAccountService bankAccountService, UserPrincipalExtractor userPrincipalExtractor, TransactionDtoValidator transactionDtoValidator) {
        this.transactionMapper = transactionMapper;
        this.bankAccountMapper = bankAccountMapper;
        this.transactionService = transactionService;
        this.bankAccountService = bankAccountService;
        this.userPrincipalExtractor = userPrincipalExtractor;
        this.transactionDtoValidator = transactionDtoValidator;
    }

    @InitBinder
    public void setupBinder(WebDataBinder binder) {
        binder.addValidators(transactionDtoValidator);
    }

    @GetMapping("/new-transaction")
    @ApiOperation("Endpoint for retrieving all bank accounts of one user.")
    public List<BankAccountDto> transaction() {
        return bankAccountMapper.toPrivateDto(bankAccountService.getBankAccountsByUserId(userPrincipalExtractor.getCurrentUser().getUserId()));
    }

    @PostMapping("/new-transaction")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation("Endpoint for performing a transaction.")
    public void transactionSubmit(@Valid TransactionDto transactionDto) {
        Transaction transaction = transactionMapper.toEntity(transactionDto);
        bankAccountService.addTransaction(transaction);
    }

    @GetMapping("/new-transaction/get-accounts")
    @ApiOperation("Endpoint for retrieving all bank account of one user")
    public List<BankAccountDto> getBankAccountsByUserId(@RequestParam("userId") Long id) {
        return bankAccountMapper.toPrivateDto(bankAccountService.getBankAccountsByUserId(id));
    }

    @GetMapping("/bank-account-transactions")
    @ApiOperation("Endpoint for retrieving all transactions of one bank account.")
    public List<TransactionDto> getTransactionsByBankAccountId(@RequestParam("id") Long id) {
        return transactionMapper.toDto(transactionService.getTransactionsByBankAccountId(id));
    }

    @GetMapping("/transactions/{id}")
    @ApiOperation("Endpoint for retrieving a transaction by id.")
    public TransactionDto getTransactionById(@PathVariable(value = "id") Long transactionId) {
        return transactionMapper.toDto(transactionService.getTransactionById(transactionId));
    }

    @GetMapping("/transactions/all")
    @ApiOperation("Endpoint for retrieving all transactions.")
    public List<TransactionDto> getAllTransactions() {
        return transactionMapper.toDto(transactionService.getAllTransactions());
    }
}

