package com.infosupport.transapption.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@ApiModel(description = "A user can be used to login and as account holder for a bank account.")
public class UserDto {

    @ApiModelProperty(example = "1", required = true, position = 1)
    @JsonProperty(access = Access.READ_ONLY)
    private Long userId;

    @ApiModelProperty(example = "User123", required = true, position = 2)
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9]+$", message = "{user.username.pattern}")
    @Size(min = 5, max = 20, message = "{user.username.size}")
    private String username;

    @ApiModelProperty(example = "PasSWoRd123!", required = true, position = 3)
    @NotBlank
    @Size(min = 8, max = 30, message = "{user.password.size}")
    private String password;

    @ApiModelProperty(example = "PasSWoRd123!", required = true, position = 4)
    @NotBlank
    private String confirmPassword;

    @ApiModelProperty(example = "ROLE_USER", position = 5)
    @JsonProperty(access = Access.READ_ONLY)
    private String role;

    /*
        Required for MapStruct
     */
    public UserDto() {

    }

    /*
        Only for testing
     */
    public UserDto(String username, String password, String confirmPassword) {
        this.username = username;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmationPassword) {
        this.confirmPassword = confirmationPassword;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

}
