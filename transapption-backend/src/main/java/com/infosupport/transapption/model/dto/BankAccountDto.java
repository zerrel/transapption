package com.infosupport.transapption.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ApiModel(description = "A bank account is required to perform transactions.")
public class BankAccountDto {

    @ApiModelProperty(example = "1", required = true, position = 1)
    @JsonProperty(access = Access.READ_ONLY)
    private Long bankAccountId;

    @NotNull
    @ApiModelProperty(example = "23", required = true, position = 2)
    @JsonIgnore
    private Long accountHolderId;

    @NotNull
    @ApiModelProperty(example = "100", required = true, position = 3)
    @Max(Long.MAX_VALUE)
    private Long balance;

    @ApiModelProperty(example = "-100", required = true, position = 4)
    @NotNull
    @Min(Long.MIN_VALUE)
    private Long minimumBalance;

    @ApiModelProperty(example = "savings", position = 5)
    @Size(max = 100, message = "{bank_account.description.size}")
    private String description;

    /*
        Required for MapStruct
     */
    public BankAccountDto() {

    }

    /*
        Only for testing
     */
    public BankAccountDto(Long accountHolderId, Long balance, Long minimumBalance, String description) {
        this.accountHolderId = accountHolderId;
        this.balance = balance;
        this.minimumBalance = minimumBalance;
        this.description = description;
    }

    public Long getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Long bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    public Long getAccountHolderId() {
        return accountHolderId;
    }

    public void setAccountHolderId(Long accountHolderId) {
        this.accountHolderId = accountHolderId;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public Long getMinimumBalance() {
        return minimumBalance;
    }

    public void setMinimumBalance(Long minimumBalance) {
        this.minimumBalance = minimumBalance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
