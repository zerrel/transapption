package com.infosupport.transapption.model.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "bank_account")
public class BankAccount {
    @Id
    @Column(name = "bank_account_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long bankAccountId;

    @NotNull
    @JoinColumn(name = "account_holder_id")
    @ManyToOne(targetEntity = User.class, cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    private User accountHolder;

    @NotNull
    private Long balance;

    @NotNull
    private Long minimumBalance;

    private String description;

    public BankAccount(){

    }

    public BankAccount(User accountHolder, Long balance, Long minimumBalance, String description) {
        this.accountHolder = accountHolder;
        this.balance = balance;
        this.minimumBalance = minimumBalance;
        this.description = description;
    }

    /*
        Only for testing
     */
    public BankAccount(Long bankAccountId, User accountHolder, Long balance, Long minimumBalance, String description) {
        this.bankAccountId = bankAccountId;
        this.accountHolder = accountHolder;
        this.balance = balance;
        this.minimumBalance = minimumBalance;
        this.description = description;
    }

    public Long getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Long bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    public User getAccountHolder() {
        return accountHolder;
    }

    public void setAccountHolder(User accountHolder) {
        this.accountHolder = accountHolder;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public Long getMinimumBalance() {
        return minimumBalance;
    }

    public void setMinimumBalance(Long minimumBalance) {
        this.minimumBalance = minimumBalance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccount that = (BankAccount) o;
        return Objects.equals(bankAccountId, that.bankAccountId) &&
                Objects.equals(accountHolder, that.accountHolder) &&
                Objects.equals(balance, that.balance) &&
                Objects.equals(minimumBalance, that.minimumBalance) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bankAccountId, accountHolder, balance, minimumBalance, description);
    }
}
