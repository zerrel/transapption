package com.infosupport.transapption.model.exception;

/*
    Throw this exception whenever a User entity cant be created in the database
 */
public class UserNotCreatedException extends RuntimeException {

    public UserNotCreatedException(String msg) {
        super(msg);
    }

    public UserNotCreatedException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
