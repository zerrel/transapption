package com.infosupport.transapption.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@ApiModel(description = "A transaction can be performed to transfer credits between two bank accounts.")
public class TransactionDto {

    @ApiModelProperty(example = "1", required = true, position = 1)
    @JsonProperty(access = Access.READ_ONLY)
    private Long transactionId;

    @ApiModelProperty(example = "22", required = true, position = 2)
    @NotNull
    private Long senderId;

    @ApiModelProperty(example = "33", required = true, position = 3)
    @NotNull
    private Long receiverId;

    @ApiModelProperty(example = "50", required = true, position = 4)
    @NotNull
    @Max(Integer.MAX_VALUE)
    @Positive(message = "{transaction.amount.positive}")
    private Integer amount;

    @ApiModelProperty(example = "31-12-2018 23:59:59", position = 5)
    private String timestamp;

    @ApiModelProperty(example = "Reason for the transaction", position = 6)
    @Size(max = 100, message = "{transaction.description.size}")
    private String description;

    /*
        Required for MapStruct
     */
    public TransactionDto() {

    }

    /*
        Only for testing
     */
    public TransactionDto(Long senderId, Long receiverId, Integer amount, String description) {
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.amount = amount;
        this.description = description;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public Long getSenderId() {
        return senderId;
    }

    public void setSenderId(Long senderId) {
        this.senderId = senderId;
    }

    public Long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(Long receiverId) {
        this.receiverId = receiverId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
