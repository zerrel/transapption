package com.infosupport.transapption.model.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "transaction")
public class Transaction {
    @Id
    @Column(name = "transaction_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long transactionId;

    @NotNull
    @JoinColumn(name = "sender_id")
    @ManyToOne(targetEntity = BankAccount.class, cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    private BankAccount sender;

    @NotNull
    @JoinColumn(name = "receiver_id")
    @ManyToOne(targetEntity = BankAccount.class, cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    private BankAccount receiver;

    @NotNull
    private Integer amount;

    // format yyyy.MM.dd.HH.mm.ss
    private String timestamp;

    private String description;

    public Transaction() {

    }

    public Transaction(BankAccount sender, BankAccount receiver, Integer amount, String description) {
        this.sender = sender;
        this.receiver = receiver;
        this.amount = amount;
        this.description = description;
    }

    /*
        Only for testing
     */
    public Transaction(BankAccount sender, BankAccount receiver, Integer amount, String timestamp, String description) {
        this.sender = sender;
        this.receiver = receiver;
        this.amount = amount;
        this.timestamp = timestamp;
        this.description = description;
    }

    /*
        Only for testing
     */
    public Transaction(Long transactionId, BankAccount sender, BankAccount receiver, Integer amount, String timestamp, String description) {
        this.transactionId = transactionId;
        this.sender = sender;
        this.receiver = receiver;
        this.amount = amount;
        this.timestamp = timestamp;
        this.description = description;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public BankAccount getSender() {
        return sender;
    }

    public void setSender(BankAccount sender) {
        this.sender = sender;
    }

    public BankAccount getReceiver() {
        return receiver;
    }

    public void setReceiver(BankAccount receiver) {
        this.receiver = receiver;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return Objects.equals(transactionId, that.transactionId) &&
                Objects.equals(sender, that.sender) &&
                Objects.equals(receiver, that.receiver) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(timestamp, that.timestamp) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionId, sender, receiver, amount, timestamp, description);
    }
}