package com.infosupport.transapption;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransapptionApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransapptionApplication.class, args);
	}

}
