package com.infosupport.transapption.mapper;

import java.util.List;

/*
    @Param D - Dto type parameter
    @Param E - Entity type parameter
 */
public interface EntityMapper<D, E> {

    E toEntity(D dto);

    D toDto(E entity);

    List<E> toEntity(List<D> dtoList);

    List<D> toDto(List<E> entityList);

}
