package com.infosupport.transapption.mapper;

import com.infosupport.transapption.model.dto.BankAccountDto;
import com.infosupport.transapption.model.entity.BankAccount;
import com.infosupport.transapption.service.UserService;
import org.mapstruct.*;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.WARN, componentModel = "spring", uses = UserService.class)
public interface BankAccountMapper extends EntityMapper<BankAccountDto, BankAccount> {

    @Override
    @Mapping(source = "accountHolderId", target = "accountHolder")
    BankAccount toEntity(BankAccountDto bankAccountDto);

    @Override
    @Mapping(target = "balance", ignore = true)
    @Mapping(target = "minimumBalance", ignore = true)
    @Mapping(source = "accountHolder.userId", target = "accountHolderId")
    BankAccountDto toDto(BankAccount bankAccount);

    /*
        Use for retrieving Bank Account of current User
     */
    @Named("privateBankAccount")
    @Mapping(source = "accountHolder.userId", target = "accountHolderId")
    BankAccountDto toPrivateDto(BankAccount bankAccount);

    /*
        Use for retrieving multiple Bank Accounts of current User
     */
    @IterableMapping(qualifiedByName = "privateBankAccount")
    List<BankAccountDto> toPrivateDto(List<BankAccount> bankAccounts);

}
