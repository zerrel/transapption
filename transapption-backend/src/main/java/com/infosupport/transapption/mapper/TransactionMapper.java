package com.infosupport.transapption.mapper;

import com.infosupport.transapption.model.dto.TransactionDto;
import com.infosupport.transapption.model.entity.Transaction;
import com.infosupport.transapption.service.BankAccountService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.WARN, componentModel = "spring", uses = BankAccountService.class)
public interface TransactionMapper extends EntityMapper<TransactionDto, Transaction>{

    @Override
    @Mapping(source = "senderId", target = "sender")
    @Mapping(source = "receiverId", target = "receiver")
    @Mapping(target = "timestamp", ignore = true)
    Transaction toEntity(TransactionDto transactionDto);

    @Override
    @Mapping(source = "sender.bankAccountId", target = "senderId")
    @Mapping(source = "receiver.bankAccountId", target = "receiverId")
    TransactionDto toDto(Transaction transaction);

}
