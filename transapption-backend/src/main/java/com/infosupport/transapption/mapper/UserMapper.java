package com.infosupport.transapption.mapper;

import com.infosupport.transapption.model.dto.UserDto;
import com.infosupport.transapption.model.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.WARN, componentModel = "spring")
public interface UserMapper extends EntityMapper<UserDto, User>{

    @Override
    @Mapping(target = "role", ignore = true)
    User toEntity(UserDto userDto);

    @Override
    @Mapping(target = "password", ignore = true)
    @Mapping(target = "confirmPassword", ignore = true)
    UserDto toDto(User user);

}
