package com.infosupport.transapption.repository;

import com.infosupport.transapption.model.entity.BankAccount;
import com.infosupport.transapption.model.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    @Query("SELECT t FROM Transaction t WHERE t.receiver.id = :id or t.sender.id = :id")
    List<Transaction> getTransactionsByBankAccountId(@Param("id") Long id);
}