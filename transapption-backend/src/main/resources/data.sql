# noinspection SqlResolveForFile

/* Create the Admin User */
INSERT INTO user (user_id, username, password, role)
VALUES(1, 'ADMIN', 'ADMIN', 'ROLE_ADMIN');

/* Add BankAccount to the Admin Account */
INSERT INTO bank_account (bank_account_id, account_holder_id, balance, minimum_balance, description)
VALUES(1, 1, 9223372036854775807, -9223372036854775808, 'Bank');
