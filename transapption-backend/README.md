# Transapption API

Transapption is a banking application created for the House of IT Project.
The application is built using Spring Boot.

## Getting Started

Run the data.sql in /src/main/resources/ to create an Admin user and a BankAccount.
The BankAccount owned by the Admin should have 1 as id.

Build the application to create the implementations of the DTO mappers.