// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import '../node_modules/bulma/css/bulma.css'
import VueResource from 'vue-resource'
import store from './components/store/index'
import axios from 'axios'
import VeeValidate from 'vee-validate'

axios.defaults.withCredentials = true

// Setting up Axios on Vue Instance, for use via this.$axios
Vue.prototype.$axios = axios
Vue.config.productionTip = false
Vue.use(VueResource)
Vue.use(VeeValidate)

axios.defaults.headers.common['Authorization'] = localStorage.getItem('token')

// axios.interceptors.response.use(response => {
//   return Promise.resolve(response)
// },
// error => {
//   if (error.response.status === 401) {
//     axios.get('http://localhost:8080/refresh', {
//       headers: { 'Authorization': this.store.getters.getToken }
//     })
//       .then((response) => {
//         console.log('getting new token: ')
//         // store.commit('setToken', 'insert token here')
//       })
//       .catch((error) => {
//         console.log('token refresh failed')
//         console.log(error)
//       })
//     return error
//   } else {
//     return Promise.resolve(error)
//   }
// })

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
