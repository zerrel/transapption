export const mutations = {
  setUser (state, payload) {
    state.user = payload
    localStorage.setItem('user', payload)
  },
  setAuth (state, payload) {
    state.isAuthenticated = payload
    localStorage.setItem('auth', payload)
  },
  setError (state, payload) {
    state.error = payload
    localStorage.setItem('error', payload)
  },
  setLoading (state, payload) {
    state.loading = payload
    localStorage.setItem('loading', payload)
  },
  setToken (state, payload) {
    localStorage.setItem('token', 'Bearer ' + payload)
    state.token = 'Bearer ' + payload
  },
  setAdmin (state) {
    if (state.user.role === 'ROLE_ADMIN') {
      state.isAdmin = true
    }
  },
  clearToken (state) {
    state.token = null
    localStorage.removeItem('token')
  },
  clearAuth (state) {
    state.auth = null
    localStorage.removeItem('auth')
  },
  clearUser (state) {
    state.user = null
    localStorage.removeItem('user')
  },
  clearAdmin (state) {
    state.isAdmin = false
    localStorage.setItem('isAdmin', 'false')
  }
}
