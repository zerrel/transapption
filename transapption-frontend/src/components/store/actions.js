import router from '@/router'
import axios from 'axios'
import jwtDecode from 'jwt-decode'
import { EventBus } from '../../event-bus'
import qs from 'qs'
export const actions = {
  userSignIn ({commit}, payload) {
    let data = {
      username: payload.username,
      password: payload.password
    }
    let options = {
      withCredentials: true,
      headers: {
        Authorization: null
      }
    }

    commit('setLoading', true)
    axios.post('http://localhost:8080/login', qs.stringify(data), options)
      .then(res => {
        commit('setAuth', true)
        commit('setLoading', false)
        commit('setError', null)
        commit('setToken', res.data.token)
        commit('setUser', jwtDecode(res.data.token))
        commit('setAdmin')
        EventBus.$emit('authenticated', 'User authenticated')
        router.push('/bank-accounts')
        router.go()
      })
      .catch(error => {
        commit('setError', error)
        commit('setLoading', false)
      })
  },
  userSignOut ({commit}) {
    commit('clearAuth')
    commit('clearToken')
    commit('clearUser')
    commit('clearAdmin')
    localStorage.clear()
    sessionStorage.clear()
    EventBus.$emit('authenticated', 'User not authenticated')
    router.push('/login')
  }
}
