export const state = {
  isAuthenticated: localStorage.getItem('auth'),
  user: null,
  isAdmin: false,
  error: null,
  loading: false,
  token: null
}
