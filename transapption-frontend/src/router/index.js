import Vue from 'vue'
import Router from 'vue-router'
import Transaction from '@/components/Transaction'
import BankAccounts from '@/components/BankAccounts'
import BankAccount from '@/components/BankAccount'
import Login from '@/components/Login'
import Register from '@/components/Register'
Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/new-transaction',
      name: 'new-transaction',
      component: Transaction
    },
    {
      path: '/bank-accounts',
      name: 'bank-accounts',
      component: BankAccounts
    },
    {
      path: '/bank-accounts/:id',
      name: 'bank-account-transactions',
      component: BankAccount,
      props: true
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: { loginPage: true, nonRequiresAuth: true }
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
      meta: { nonRequiresAuth: true }
    }
  ]
})

router.beforeEach((to, from, next) => {
  const requiresAuth = !to.matched.some(record => record.meta.nonRequiresAuth)
  const isLoginPage = to.matched.some(record => record.meta.loginPage)
  const isAuthenticated = localStorage.getItem('auth')
  if (requiresAuth && !isAuthenticated) {
    next('/login')
  } else if (isLoginPage && isAuthenticated) {
    router.push('/bank-accounts')
  }
  next()
})

export default router
